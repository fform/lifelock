## Get The Maxmind DB

wget http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz
gunzip GeoLite2-City.mmdb.gz /web/fuel/app/cache/

## Commands to run on fresh install

npm install
bower install
composer install
php oil r migrate

## Reverse Proxy for OAuth Testing

ngrok -subdomain=lifelock-tmi 80

## Commands when configuring a fresh app
php oil r session:create
php oil r migrate --packages=auth