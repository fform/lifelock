if (!window.console) {
	window.console = {
		log: function () {},
		warn: function () {},
		error: function () {}
	};
}
window.App = (function (w, $) {
	_onResize = [];
	var App = {
		"env": "",
		"currentPage": "",
		"baseurl": "",
		"DEV": "development",
		"STAGE": "staging",
		"PROD": "production",
		fn: {},
		"extend": function (id, cb) {
			if (App.fn.hasOwnProperty(id)) {
				App.error(id, "Overwrite namespace");
				return;
			}
			App.fn[id] = cb;
		},
		"init": function (env, base) {
			App.env = env || App.DEV;
			if (!w.location.origin)
				w.location.origin = w.location.protocol + "//" + w.location.host;
			App.baseurl = base || w.location.origin;
			App.currentPage = '/' + String(document.location.href).replace(App.baseurl, '').replace(/#+.*/ig, '');

			$(App.ready);
			$(window).on('resize.app', onResizeCaller);
			onResizeCaller();
		},
		"config": function (key) {
			if (Config && Config[key]) {
				return Config[key];
			}
			return undefined;
		},
		"ready": function () {
			for (var i in App.fn) {
				if (App.fn[i] && App.fn[i].hasOwnProperty('init') && typeof App.fn[i].init === "function") {
					App.fn[i].init();
				}
				if (App.fn[i] && App.fn[i].hasOwnProperty('page') && App.fn[i].hasOwnProperty('match') && typeof App.fn[i].page === "function" && App.fn[i].match.test(App.currentPage)) {
					App.fn[i].page();
				}
			}
		},
		"base": function (append) {
			return App.baseurl + (append || "");
		},
		"log": function () {
			if (App.env === App.DEV) {
				console.log.apply(console, arguments);
			}
		},
		"error": function () {
			if (App.env === App.DEV) {
				console.error.apply(console, arguments);
			}
		},
		"page": function (id) {
			App.currentPage = id;
		},
		"onsize": function (fn) {
			_onResize.push(fn);
			onResizeAction(fn);
		}
	};

	var onResizeCaller = function () {
		for (var i in _onResize) {
			onResizeAction(_onResize[i]);
		}
	};
	var onResizeAction = function (fn) {
		var size = {
			w: $(window).width(),
			h: $(window).height(),
			header: {
				h: $(".header").height(),
				w: $(".header").width()
			}
		};
		fn(size);
	};
	return App;
})(window, jQuery);