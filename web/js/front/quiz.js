App.extend('quiz', (function (w, $) {
	var _questions = [];
	var _user;
	var _lastCard = 0;
	var _totalCards = 0;
	var _perRound = 10;
	var _roundStart = 0;
	var _roundEnd = 0;
	var _showPop = true;
	var _firstPop = true;
	var _moreTimes = 0;
	var _welcomeCards = [];
	var TIME = 400;
	var DISTANCE = 150;
	var TRYURL = "https://secure.lifelock.com/enrollment";

	var plugin = {
		match: /\/quiz.*/i,
		page: function () {
			console.log('loading quiz');
			TRK.bench('state');
			TRK.bench('leaving');
			TRK.bench('rounds');
			TRK.event('Loading Quiz');


			transitionCardIn();
			$.get(App.base('api/index.json?count=50'), function (d) {
				var html = "";
				var total = 0,
					fails = 0,
					oks = 0;
				_user = d.user;
				for (var i in d.questions) {
					var question = d.questions[i];
					if (question && question.hasOwnProperty('type') && question.type === 'welcome') {
						_welcomeCards.push(question);
						continue;
					}

					total += 1;
					if (question && question.passed) {
						_questions.push(question);
						oks += 1;
					} else {
						fails += 1;
					}

				}

				TRK.bench('state', "Loaded");
				TRK.increment_lifetime("Played");
				TRK.event('Loaded Questions', total + " questions, " + oks + " OK, " + fails + " BAD");
				TRK.event('Total Questions', total);
				TRK.event('Total OK', oks);
				TRK.event('Total BAD', fails);
				console.log('%s Questions, %s OK, %s BAD', total, oks, fails);
				_totalCards = oks;

				startRound(_lastCard, Math.min(_perRound, oks));


			});

			$("#pop-try").on('click', function () {
				TRK.event('Clicked', "Try Lifelock from Popover");
				//plugin.leaving();
			});
			$("#btn-endgame").on('click', function () {
				TRK.event('Clicked', "End Game");
				plugin.leaving();
			});
			$("#btn-getlifelock").on('click', function () {
				TRK.event('Clicked', "Get Lifelock");
				return true;
				//plugin.leaving();
			});
			$("#pop-return").on('click', function () {
				TRK.event('Clicked', "Return to TMI from Popover");
				togglePopover(false);
			});
			$(".bug").on('click', function () {
				TRK.event('Clicked', "Open Popover Promo");
				togglePopover(true);
			});
			$("#popover").on('click', function (e) {
				e.stopPropagation();
			});
		},

		drawQuestion: function (id, welcomeCard) {
			var question = (welcomeCard !== undefined ? _welcomeCards[welcomeCard] : _questions[id]);
			var isWelcome = (question.type === "welcome");
			var html = "";
			var classes = [question.layout];
			var card = "<p class='challenge'>" + question.text + "</p>";
			if (question.image) {
				classes.push('image');
				html += "<img src='" + question.image + "' class='pin'>";
			}
			if (question.image_set) {
				classes.push('image-set');
				html += "<ul >";
				var set_count = 1;
				for (var j in question.image_set) {
					if (set_count > 3) {
						continue;
					}
					var img = question.image_set[j];
					html += "<li><img src='" + img + "' height='100'></li>";
					set_count++;
				}
				html += "</ul>";
			}

			if (question.layout === "scenario") {
				card += "<img src='" + App.base('assets/img/elements/notice.png') + "' class='notice'>";
			}

			if (isWelcome) {
				var mt = _moreTimes + 1;
				question.explain = String(question.explain).replace("%s", (mt === 1 ? '' : 's'));
				question.explain = String(question.explain).replace("%d", "<span class='answer'>" + mt + "</span>");
			}
			card += "<p class='explain'>" + question.explain + "</p>";
			card += "<div class='btns'>";
			if (question.layout === "qa") {
				card += "<a href='#' class='button continue space qa yes'><span>Yes</span></a>";
				card += "<a href='#' class='button continue qa no'><span>No</span></a>";
				card += "<div class='hide explain-open'><a href='#' class='button continue'><span class='rpad'>Continue</span>tmi</a></div>";
			} else {
				card += "<a href='#' class='button continue'><span class='rpad'>Continue</span>tmi</a>";
			}
			card += "</div>";

			//Hello " + _user + "
			var headerCopy = "tmi #" + (_lastCard - _roundStart + 1) + " of " + (_roundEnd - _roundStart);
			if (isWelcome) {
				headerCopy = "";
			}
			html += "<div class='header'><span>" + headerCopy + "</span></div>";
			html += "<div class='inner'>" + card + "</div>";

			transitionCardOut(function () {
				$("#quiz").html("<div class='card " + classes.join(' ') + "'>" + html + "</div>");
				if (matchMedia(Foundation.media_queries.medium).matches === true && $(w).height() < 800) {
					$(".card").css({
						'top': 40,
						'height': ($(w).height() * 0.6),
						'width': ($(w).width() * 0.6)
					});
				} else {
					var wh = $(w).height();
					var available_h = $(window).height() - (($(window).height() - $("#app").height()) + $("#popover").height() + $("#quiz").offset().top);

					var nh = Math.min(available_h, ($(".card .inner").height() + 30));
					var setTop = 10;
					$(".card").css({
						top: setTop,
						height: nh,
						'max-height': nh
					});
					$(".card .inner").css({
						height: (nh - 30)
					});
				}

				$("#quiz .card a.button").on('click', function () {
					if ($(this).hasClass('qa')) {
						$card = $(".card");

						if (!$(this).hasClass('yes')) {
							$(".explain", $card).prepend("You just helped us narrow down the right person. ");
						}

						$(".inner p", $card).slideUp();
						$(".inner .button.qa", $card).slideUp();
						$(".explain-open", $card).slideDown();
						$(".explain", $card).slideDown();
					} else {
						plugin.nextCard(isWelcome);
					}

				});
				if (question.layout === 'qa') {
					$(".explain").hide();
				}
				transitionCardIn();
			});
			//console.log('activate crumb', id);
			$("#crumb_" + id).addClass('active');
			if (!isWelcome) {
				$("#bread").show();
			}
		},

		nextCard: function (fromWelcome) {

			_lastCard += (fromWelcome ? 0 : 1);
			//console.log('question %d/%d', _lastCard, (_roundStart + _perRound));
			if (_lastCard === (_roundStart + _perRound) || _lastCard >= _totalCards) {
				//done
				TRK.bench('state', "Round End");
				console.log('done');
				//_lastCard -= 1;
				$("#bread").hide();
				plugin.showResults();
			} else {
				TRK.bench('state', "Next Card:" + _lastCard);
				//console.log('next');
				if (_lastCard === 2 && _showPop) {
					_showPop = false;
					togglePopover(true, true);

				}

				plugin.drawQuestion(_lastCard);
				var percentageComplete = (_lastCard - _roundStart) / _perRound;
				App.fn.progress.setProgress(percentageComplete - 0.5);
				var nextImg = (_lastCard + 1) % 31; // Total backgrounds possible
				loadBackground(nextImg);

			}
		},

		leaving: function () {
			TRK.bench('leaving', "Toggled");
			var html = $("#template_leaving").html();

			function OpenInNewTab(url) {
				var win = window.open(url, '_blank');
				win.focus();
			}
			App.fn.modal.open(html, {
				learnmore: function () {
					console.log('learn');
					TRK.event('Clicked', "Learn More");
					OpenInNewTab(TRYURL);
				},
				tryit: function () {
					console.log('try');
					TRK.event('Clicked', "Try Lifelock");
					OpenInNewTab(TRYURL);
				},
				height: 0.8,
				id: 'leaving'

			});
		},

		showResults: function () {
			toggleExpanded(true);
			TRK.bench('rounds', "Show Round SUmmary");
			console.log('Results per:%d card:%d total:%d left:%d rounds:', _perRound, _lastCard, _totalCards, (_totalCards - _lastCard), Math.ceil(((_totalCards - _lastCard) / _perRound)));
			transitionCardOut();
			loadBackground(0);
			$("#popover").removeClass('collapse');
			$("body").addClass('results-up');
			var $html = $($("#template_final").html());


			App.fn.progress.setProgress(0.5);
			$(".social-small", $html).on('click', function (e) {
				TRK.event('Clicked', "Social share");
				var action = $(this).data('action');
				TRK.event('Social', action);
				// Do something with a social share
			});
			$("a.social-small", $html).each(function (i, e) {
				$(this).attr('href', App.fn.index.socialUrl($(this).data('action'), {
					title: App.config('meta').title,
					desc: App.config('meta').description,
					image: App.base('assets/img/badges/1.jpg'),
					url: App.base()
				}));
			});

			$(".video img", $html).on('click', function () {
				TRK.event('Clicked', "Video");
				$(document).scrollTop(0);
				var html =
					'<div class="flex-video">' +
					'<iframe width="560" height="315" src="//www.youtube.com/embed/R9d4qWJiy-I?list=PLJMEwhMAMRH9oj1DASwxF25Fk18j7pxI8&autoplay=1" frameborder="0" allowfullscreen autoplay></iframe>' +
					'</div>';
				App.fn.modal.open(html, {
					learnmore: function () {
						console.log('learn');
						TRK.event('Clicked', "Learn More");
					},
					tryit: function () {
						console.log('try');
						TRK.event('Clicked', "Try Lifelock");
					},
					height: 0.7,
					id: 'video'

				});
			});

			$("#btn-final-try", $html).on('click', function () {
				TRK.event('Clicked', "Try Lifelock from round summary");
			});

			$("#btn-results", $html).on('click', function () {
				TRK.event('Clicked', "View Results");
				$("#results .inner").html('');
				var question_no = 0;
				$(_questions).each(function (i, e) {
					var question = e;
					if ((question.type && question.type === 'welcome') || i >= _roundEnd) {
						return;
					}
					question_no += 1;

					var $r = $($("#template_result").html());

					$(".question", $r).html("<div class='vcenter'><p class='qnumber'>TMI #" + question_no + " of " + _roundEnd + "</p><p>" + question.text + "</p></div>");
					$(".explain", $r).html(question.social);
					$("#results .inner").append($r);
					$("a.social-small", $r).each(function (i, e) {
						$(this).attr('href', App.fn.index.socialUrl($(this).data('action'), {
							title: App.config('meta').title,
							desc: question.social,
							image: App.base('assets/img/badges/1.jpg'),
							url: App.base()
						}));
					});

				});
				var resultBtns = "#result-title,#result-play,#result-final";
				$(resultBtns).show();
				if (_moreTimes) {
					$("#result-play").off('click').on('click', function (e) {
						TRK.event('Clicked', "Play again from results");
						$("#final").remove();
						$("#results").hide();
						$(resultBtns).hide();
						$("#popover").addClass('collapse');
						startRound();
						e.preventDefault();
					});
				} else {
					$("#result-play").hide();
				}
				$("#result-final").off('click').on('click', function (e) {
					TRK.event('Clicked', "Return to round summary");
					e.preventDefault();
					$("#results").hide();
					$(resultBtns).hide();
					$("#final").show();
				});

				$("#results").show();
				$("#results .result-row").each(function () {
					var newHeight = Math.max($(".question", this).height(), $(".result", this).height());
					$('.question', this).height(newHeight);
					$('.result', this).height(newHeight);
				});


				$("#final").hide();
			});
			if (_moreTimes) {
				$("#play-again a", $html).on('click', function () {
					console.log('next round');
					TRK.event('Clicked', "Play another round");
					$("#final").remove();
					$("#popover").addClass('collapse');
					startRound();

				});

				$("#play-again p", $html).html("We've only just begun. You can play <strong>tmi</strong> <span class='more-times'>" + _moreTimes + " more time" + (_moreTimes > 1 ? 's' : '') + "</span> with the info available about you online.");
			} else {
				TRK.bench('rounds', "No more rounds");
				$("#play-again", $html).html('');
				$($html).addClass('nomore');
			}


			$(".score", $html).html("Dynamic Score");
			$(".space", $html).css({
				left: -50,
				opacity: 0
			});
			$("body").append($html);
			$(".space", $html).transition({
				left: 0,
				opacity: 1
			});


		}
	};

	var startRound = function () {
		toggleExpanded(false);
		_roundStart = _lastCard;
		_roundEnd = Math.min(_lastCard + _perRound, _totalCards);
		_moreTimes = Math.floor(((_totalCards - _lastCard) / _perRound));
		console.log('start round', _lastCard, _roundEnd);
		var crumbs = "";
		for (var i = _roundStart; i < _roundEnd; i++) {

			crumbs += "<div class='crumb' id='crumb_" + i + "'></div>";

		}
		$("#bread").html(crumbs).hide();
		plugin.drawQuestion(_roundStart, (Math.floor(_roundStart / _perRound) % 2));

		$("body").removeClass('results-up');
	};

	var transitionCardIn = function (callback) {
		var left_distance = DISTANCE;
		var cb = callback || function () {};

		if ($("#quiz .card").hasClass('transparent')) {
			left_distance = 0;
		}

		$("#quiz .card").css({
			opacity: 0,
			left: (-1 * left_distance)
		}).transition({
			left: 0,
			opacity: 1
		}, TIME, function () {
			cb();
		});

	};

	var transitionCardOut = function (callback) {
		var left_distance = DISTANCE;
		var cb = callback || function () {};

		if (!$("#quiz .card").length) {
			$("#quiz").removeClass('loading');
			cb();
		} else {
			if ($("#quiz .card").hasClass('transparent')) {
				left_distance = 0;
			}
			$("#quiz .card").transition({
				opacity: 0,
				left: left_distance
			}, TIME, function () {
				$(this).remove();
				$("#quiz").removeClass('loading');
				cb();
			});
		}

	};

	var togglePopover = function (open, firstTime) {
		if (open) {
			var classes = 'collapse ';
			if (!firstTime) {
				classes += 'expand opening';
				setTimeout(function () {
					$("#app").on('click.popover', function () {
						togglePopover(false);
					});
				}, 0);
			} else {
				$("#app").addClass('popped');
			}

			$("#popover").addClass(classes);

		} else {
			$("#popover").removeClass('expand');
			$("#app").off('click.popover');

		}
	};

	var toggleExpanded = function (open) {
		if (open) {
			$("#app").addClass('expand');
		} else {
			$("#app").removeClass('expand');
		}
	};

	var loadBackground = function (id) {
		var $img = $("<img>").addClass('fit').attr('src', App.base('assets/img/bg/slides/' + id + ".jpg"));
		$img.on('load', function () {
			$("#bg img.old").transition({
				opacity: 0
			}, TIME, function () {
				$(this).remove();
			});
			$img.css({
				opacity: 0
			});
			$("#bg").append($img);
			$("body").trigger('resize');
			$img.transition({
				opacity: 1
			}, TIME, function () {
				$(this).addClass('old');
				$("body").trigger('resize');
			});
		});
	};

	return plugin;

})(window, jQuery));