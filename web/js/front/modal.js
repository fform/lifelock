App.extend('modal', (function (w, $) {
	var $bg,
		$modal,
		_config = {};

	var plugin = {
		init: function () {
			$("body").addClass('md-body-setup');
			$modal = $("<div class='modal'>").html("<div class='inner'><a class='close'><span><i class='icon icon_cross'></i></span></a><div class='content vcenter'></div></div>");
			$bg = $("<div class='modal-bg md-effect-flip-vert'>").append($modal);
			$("a.close", $modal).on('click', function () {
				plugin.close();
			});
			$bg.on('click', function () {
				plugin.close();
			});
			$modal.on('click', function (e) {
				e.stopPropagation();
			});

			$modal.on('keyup', ".content .enter-sub", function (e) {
				var key = e.which || e.keyCode;
				var action = $(this).data('action');
				if (key === 13) {
					if (typeof _config[action] === "function") {
						_config[action](function (r) {
							if (r) {
								plugin.close();
							}
						});
					}
				}
			});
			$modal.on('click', ".content a.button", function (e) {
				e.preventDefault();
				var action = $(this).data('action');
				if (plugin.hasOwnProperty(action) && typeof plugin[action] === "function") {
					plugin[action]();
				}

				if (typeof _config[action] === "function") {
					var result = _config[action](function (r) {
						if (r) {
							plugin.close();
						}
					});
					if (result) {
						plugin.close();
					}
				}
			});
		},

		open: function (msg, config) {
			if (msg) {
				$(".content", $modal).html(msg);
			}
			_config = config || {};
			$modal.hide();
			if (_config.id) {
				$modal.addClass(_config.id);
			}
			var wh = $(window).height();
			$(".inner", $modal).height((wh * (_config.height || 0.3)));
			$("body").append($bg);
			$modal.fadeIn();
			$bg.addClass('show');
			$("body").addClass('show');
			$(window).on('scroll', function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				return false;
			});
		},

		close: function () {
			$bg.removeClass('show').fadeOut(250, function () {
				$bg.detach();
				$bg.show();
			});
			$("body").removeClass('show');
			if (_config.id) {
				$modal.removeClass(_config.id);
			}
			$(window).off('scroll');
		},

		alert: function (msg) {
			App.fn.modal.open("<p>" + msg + "</p><a href='#' class='button' data-action='close'>Ok</a>");
		}
	};


	return plugin;

})(window, jQuery));