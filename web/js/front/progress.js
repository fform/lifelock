App.extend('progress', (function (w, $) {
	var $el;
	var PWIDTH = 3000;
	var PHEIGHT = 949;
	var PMAX = 560;
	var _lastProgress = 0;

	var plugin = {
		init: function () {
			$el = $("#progress");

			App.onsize(function () {
				setProgress();
			});
			setTimeout(function () {
				setProgress();
			}, 100);
		},
		setProgress: function (to) {
			setProgress(to, true);
		}
	};

	var getVal = function () {
		return (parseFloat($el.data('val')) / 100);
	};

	var setProgress = function (val, animated) {
		var p = _lastProgress;
		if (val !== undefined) {
			p = val;
		}

		var ww = $("#app").width();
		var wh = $("#app").height();
		var ratio = wh / PHEIGHT;
		var newWidth = Math.round(ratio * PWIDTH);
		var margins = ww * 0.25;
		var offset = (-1 * newWidth) + ((ww - (margins * 2)) * p) + margins + (PMAX * ratio);
		var newVal = {
			'left': offset,
			'height': wh,
			'width': newWidth
		};

		if (animated) {
			$("#progress img").transition(newVal);
		} else {
			$("#progress img").css(newVal);
		}

		_lastProgress = p;
	};

	return plugin;

})(window, jQuery));