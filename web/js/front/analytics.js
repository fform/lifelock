App.extend('analytics', (function (w, $) {
	var _active = false,
		_start = 0,
		_timers = {};

	var plugin = {

		init: function () {
			_active = w.hasOwnProperty('dataLayer');
			_start = timestamp();
		},

		event: function (event_name, value) {
			var payload = {
				'event': event_name,
				'elapsed': ((timestamp() - _start) / 1000)
			};
			if (value) {
				payload.value = value;
			}
			console.log('Event', payload);
			pushPayload(payload);
		},

		obj: function (obj) {
			pushPayload(obj);
		},

		bench: function (key, remark) {
			var ts = timestamp();
			if (_timers.hasOwnProperty(key)) {
				var rm = remark || "";
				pushPayload({
					'event': "bench",
					'benchmark': key,
					'start': _timers[key],
					'end': ts,
					'elapsed': ((ts - _timers[key]) / 1000),
					'remark': rm
				});
				console.log("Marked", key, remark, ((ts - _timers[key]) / 1000));
			}

			_timers[key] = ts;
		},

		increment_lifetime: function (part) {
			var key = 'LL_LT_' + String(part).toLowerCase();
			var config = {
				path: '/'
			};
			var count = 0;
			if ($.cookie(key)) {
				count = parseInt($.cookie(key), 10);
			}
			count += 1;
			plugin.event("Lifetime " + part, count);
			$.cookie(key, count, config);
		}
	};

	var pushPayload = function (payload) {
		if (_active) {
			w.dataLayer.push(payload);
		}
		return w.dataLayer;
	};

	var timestamp = function () {
		return new Date().getTime();
	};

	return plugin;

})(window, jQuery));