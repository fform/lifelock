App.extend('index', (function (w, $) {
	var plugin = {
		match: /^\/$/i,
		page: function () {

			App.fn.progress.setProgress(-0.25);

			$.ajax({
				dataType: 'json',
				url: App.base('api/teaser.json'),
				success: function (d) {
					var choice = Math.floor(Math.random() * d.list.length);
					$("#geospace").append(d.list[choice]);

					$("#warning").transition({
						opacity: 1,
						bottom: 0
					});

					// This resolves issue where the weather question for some reason
					// must display forecast all in lower case
					if (choice === 0) {
						var sml = String($("#geospace span:first").text()).toLowerCase();
						$("#geospace span:first").text(sml);
					}
					// end this
				}
			});

			App.onsize(function (d) {
				$("#app").height(d.h);
			});
			$(".roller .item").on('mouseover', function () {
				var dir = $(this).parent().data('roll');
				var $item = $(this);
				var tool_width = 368;
				$("#rolls-" + dir).css({
					position: 'absolute',
					display: 'block',
					opacity: 0,
					right: -1 * tool_width + 50,
					top: (($item.height() / 2) - 70)
				}).transition({
					opacity: 1
				}, 250);
			}).on('mouseout', function () {
				var dir = $(this).parent().data('roll');

				$("#rolls-" + dir).transition({
					opacity: 0
				}, 250, function () {
					$(this).hide();
				});
			});
		},
		init: function () {

			$(".fitter").each(function (i, e) {
				plugin.matchHeight(e, (i === 0));
			});

			$("#home").transition({
				opacity: 1
			});

		},

		matchHeight: function (selector, includeHeader) {
			var $sel = $(selector);
			var head = includeHeader;
			App.onsize(function (s) {
				var newHeight = s.h;
				var min_height = $sel.data('min') || 0;
				if (head) {
					newHeight -= s.header.h;
				}
				$sel.css({
					'height': newHeight
				});

				$("img.fit", $sel).each(function (i, e) {
					var img = e;

					getOriginalDimOfImg(img, function (orig) {
						var newWidth = orig.w * (newHeight / orig.h);
						var newOffset = {
							'min-height': min_height
						};

						if ($(img).hasClass('ud')) {
							newOffset = {
								left: 'auto',
								right: 0,
								top: 0
							};
						} else {
							if (newWidth < $sel.width()) {
								newWidth = $sel.width();
								newHeight = orig.h * (newWidth / orig.w);
							}
							newOffset.left = ($sel.width() - newWidth) / 2;
						}

						$(img).height(newHeight).css(newOffset);
					});

				});
			});
		},


		socialUrl: function (action, config) {
			var link = '';
			var title = encodeURIComponent(config.title),
				description = encodeURIComponent(config.desc),
				url = encodeURIComponent(config.url),
				image = encodeURIComponent(config.image);
			switch (action) {
			case 'fb':
				link = "https://www.facebook.com/dialog/feed?app_id=" + App.config('fbid') +
					"&link=" + url +
					"&name=" + title +
					"&caption=" + description;
				//link = "http://www.facebook.com/sharer.php?s=100&p[title]=" + title + "&p[summary]=" + description + "&p[url]=" + url;
				if (config.image) {
					link += "&picture=" + image;
				}
				link += "&redirect_uri=" + "https://www.facebook.com";
				break;
			case 'tw':
				link = "http://twitter.com/share?url=" + url + "&text=" + description;
				break;
			case 'tu':
				link = "http://www.tumblr.com/share/link?url=" + url + "&name=" + title + "&description=" + description;
				break;
			case 'gp':
				link = "https://plus.google.com/share?url=" + url;
				break;
			case 'em':
				link = "mailto:?subject=" + title + "&body=" + url;
				break;
			}

			return link;
		}
	};

	function getOriginalDimOfImg(img_element, cb) {
		var getDim = function () {
			var t = new Image();
			t.src = (img_element.getAttribute ? img_element.getAttribute("src") : false) || img_element.src;
			return {
				w: t.width,
				h: t.height
			};
		};
		var d = getDim();
		if (d.w !== 0 || d.h !== 0) {
			cb(d);
		} else {
			$(img_element).on('load', function () {
				cb(getDim());
			});
		}
	}

	return plugin;

})(window, jQuery));