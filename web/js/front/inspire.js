App.extend('inspire', (function (w, $) {
	var plugin = {
		init: function () {
			$("#inspire-source").hide();
			$("#inspire-source li").each(function (i, e) {
				var $thumb = $(".excerpt img:first", e);
				var $large = $(".content img:first", e);

				var li = $("<li>").append($("<a>").attr('href', $large.attr('src')).append($("<img>").attr('src', $thumb.attr('src'))));

				$("#inspire").append(li);
			});
			$("#inspire-source").remove();
			$("#inspire").foundation();
		}
	};


	return plugin;

})(window, jQuery));