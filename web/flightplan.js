var Flightplan = require('flightplan');
var plan = new Flightplan();
var buildName, tmpDir, buildData, whoami, gitRevision, gitCommitMsg;
var fs = require('fs');

var remotePHP, remotePath;
var paths = {
	'production': '/var/www/lifelock/',
	'staging': '/var/www/fform/lifelock/'
};

plan.briefing({
	debug: false,
	destinations: {
		'staging': [{
			host: 'fform.com',
			username: 'www-data',
			agent: process.env.SSH_AUTH_SOCK
		}],
		'production': [{
			host: '23.239.20.245',
			username: 'www-data',
			agent: process.env.SSH_AUTH_SOCK
		}]
	}
});

plan.local(['default', 'clean'], function (local) {
	local.log("Setup Vars");
	local.silent();
	remotePath = paths[plan.target.destination];
	remotePHP = "APP_ENVIRONMENT=" + plan.target.destination + " php ";
	gitRevision = local.exec('git log -n 1 --pretty=format:"%H"').stdout;
	gitCommitMsg = local.exec('git log -n 1 --pretty=oneline').stdout.replace(gitRevision, '');
	buildName = plan.target.destination + '-' + new Date().getTime();
	whoami = local.exec("git config user.email").stdout;
	local.verbose();
	local.log("Remote Path: ", remotePath);
});

plan.local(function (local) {

	local.log('Run build ' + buildName + ' : ' + remotePHP);
	//local.exec("rm -rf app/fuel/app/logs && mkdir app/fuel/app/logs && touch app/fuel/app/logs/.gitkeep");

	local.log('Copy files to remote hosts');
	var filter = "-type f | grep '.git|tests|test/|.DS_Store|files|cache/' -vE";

	var files_fuel = local.exec("find ./fuel " + filter, {
		silent: true
	}).stdout.split('\n');
	var files_public = local.exec("find ./public " + filter, {
		silent: true
	}).stdout.split('\n');
	var filesToCopy = files_fuel.concat(files_public);
	filesToCopy.push('./oil');

	local.silent();
	var t = local.transfer(filesToCopy, remotePath, {
		silent: true
	});
	local.verbose();
});

plan.remote('clean', function (remote) {
	remote.log("Cleaning ...");
	remote.exec(remotePHP + remotePath + "oil r deploy");
	remote.log("done.");
});

plan.remote(function (remote) {
	var remotePath = paths[plan.target.destination];
	remote.exec(remotePHP + remotePath + "oil r migrate");

	var htaccess = remotePath + '/public/.htaccess';
	remote.exec('echo "SetEnv APP_ENVIRONMENT ' + plan.target.destination + '" | cat - ' + htaccess + ' > ' + htaccess + '.tmp');
	remote.exec('rm ' + htaccess + " && mv " + htaccess + ".tmp " + htaccess);

	//remote.exec('wget -N http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz -P ' + remotePath);
	//remote.exec('gunzip ' + remotePath + 'GeoLiteCity.dat.gz');

});

plan.local(function (local) {
	local.log("Ping Rollbar with deploy");
	local.exec("curl https://api.rollbar.com/api/1/deploy/ -d 'access_token=acc66b66214046a5ac67b59307d97dc7' -d 'environment=" + plan.target.destination + "' -d 'revision=" + gitRevision + "' -d 'comment=" + encodeURIComponent(gitCommitMsg) + "' -d 'local_username=" + encodeURIComponent(whoami) + "'", {
		silent: true
	});
});