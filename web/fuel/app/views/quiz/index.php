
<div id="app">
	<div id="topbar">
		<div id="logo">
			<?= Asset::img('elements/tmi-logo-white.png', array('class'=>'')); ?>
		</div>
		<div id="topnav">
			<ul class="inline-list">
				<li><a id="btn-endgame" href="#">End Game</a></li>
				<li><a id="btn-getlifelock" href="https://secure.lifelock.com/enrollment" target="_blank">Get Lifelock</a></li>
			</ul>
		</div>
		<div id="bread">
		</div>
		<h1 id="result-title" class='alt white lower'>Results Summary</h1>
		<a id="result-play" href="<?=Uri::base();?>/quiz" class="button continue blue-over">Play Again</a>
		<a id="result-final" href="<?=Uri::base();?>/quiz" class="button continue blue-over"><span class='rpad'>Return To</span>tmi</a>
	</div>
	<div id="bg" class='fitter' data-min='850'>
		<?= Asset::img('bg/home.jpg', array('class'=>'fit old')); ?>
	</div>
	<div id="progress" data-val="0">
		<?= Asset::img('elements/progress.png'); ?>
	</div>
	<div id="quiz" class='loading'>
		<div class="card transparent">
			<div class="inner">
				<div class="pre-loader"></div>
			</div>
		</div>
	</div>
	<div id="popover">
		<div class="arrow"></div>
		<div class="bug">Get your free 30-day trial* of LifeLock.</div>
		<div class="inner">
			<p>Congrats! You've earned a free 30-day trial of LifeLock just for playing TMI. Now that you know how online crooks can operate, let's get you on the road to protection.</p>
			<a id="pop-try" href="https://secure.lifelock.com/enrollment" target="_blank" class="button success continue no-bot blue-over"><span>Try Lifelock</span></a>
			<p>Free for 30 Days*</p>
			<p><a id="pop-return" href="#" class="button continue blue-over"><span class='rpad'>Return To</span>tmi</a></p>
			<div class="disclaimer">
				* At the end of the 30-day trial period, your card will be billed automatically ($9.99mo/$109.89yr plus applicable sales tax for LifeLock Standard™ protection, $19.99mo/$219.89yr plus applicable sales tax for LifeLock Advantage™ protection, $29.99mo/$329.89yr plus applicable sales tax for LifeLock Ultimate Plus™ protection) unless you cancel within the 30-day trial period. You can cancel anytime without penalty by calling 1-800-LifeLock.
			</div>
		</div>
	</div>
	<div id="results">
		<div class="row hide-for-small">
			<div class="columns medium-6 know-share">We know this...</div>
			<div class="columns medium-6 know-share">But you'll only share this.</div>
		</div>
		<div class="row">
			<div class="columns large-12 large-centered inner">
				
			</div>
		</div>
	</div>
</div>

<div class='footer'>
	<?= Asset::img('elements/lifelock-tag.png', array('class'=>'bug')); ?>
	<div class="inner text-center bug-bar">
		<div class="clearfix">
		<div class='share-copy'>share <span class='tmi-word'>tmi</span> with friends.</div>
		<div class="left fb"><div class="fb-like" data-href="<?=Uri::base();?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div></div>
		<div class="left gp"><div class="g-plusone" data-size="medium"></div></div>
		<div class="left tw"><a href="https://twitter.com/share" class="twitter-share-button" data-text="<?=Lang::get('site.social-share');?>">Tweet</a></div>
		</div>
	</div>
	<div class=" text-center legal">
		<p>
			<?= Lang::get('site.legal.top'); ?><br>
			<?= Lang::get('site.legal.bottom'); ?>
		</p>
	</div>
	<? if(Fuel::$env !== Fuel::PRODUCTION): ?><a href="<?=Uri::base();?>task/logout">Logout</a><? endif; ?>
</div>

<script id="template_result" type="text/html">

<div class="row result-row">
	<div class="columns small-12 show-for-small know-share">
		We know this...
	</div>
	<div class="columns medium-6 question">
		You're related to <span class="answer">thing</span>
	</div>
	<div class="columns small-12 show-for-small know-share">
		But you'll only share this.
	</div>
	<div class="columns medium-6 result">
		<div class="arrow"></div>
		<div class="explain">officia perspiciatis quas, reiciendis est modi sunt adipisci cumque quidem ullam omnis provident error sit aliquam, rerum?</div>
		<h5 class='alt'>Help Protect your friends
			<div class="right">
					<ul class="inline-list">
						<li><a href="#" target="_blank" class='social-small fb' data-action="fb"></a></li>
						<li><a href="#" target="_blank" class='social-small tw' data-action="tw"></a></li>
						<li><a href="#" target="_blank" class='social-small gp' data-action="gp"></a></li>
						<li><a href="#" target="_blank" class='social-small tu' data-action="tu"></a></li>
					</ul>
				</div>
		</h5>
		<div class="detail">This message lets you tell your friends about TMI. Only the above message will be shared. We will not share your personal info.</div>
		
	</div>
</div>
</script>

<script id="template_leaving" type="text/html">
<div class="dashbox">
	<h3 class="blue">Leaving so soon?</h3>
	<?= Asset::img('elements/tmi-logo.png', array('class'=>'')); ?>
	<p>If you're exiting because you don't want us to access your information…good for you! After all, you can never be too careful. That's why so many people turn to LifeLock to protect their finances, credit, and good name.</p>
</div>
<div class="text-center">
<a data-action="tryit" href="https://secure.lifelock.com/enrollment" target="_blank" class="button success continue no-bot blue-over"><span>Try Lifelock</span></a>
<p>Free for 30 days</p>
<p><a data-action="close" href="#" class="button continue"><span class='rpad'>Return to</span>tmi</a></p>
<a data-action="learnmore" href="https://secure.lifelock.com/enrollment" target="_blank" class="standard blue-over"><span>Learn More about Lifelock</span></a>
</div>
</script>

<script id="template_final" type="text/html">
<div id="final">
	<div class="space">
		<div class="larrow"></div>
		<div class="rarrow"></div>
		<div class="inner">
			<h3>Thanks for playing <span class='tmi-word'>tmi</span>!</h3>
			<p class='earned'>
				You've earned the Identity Theft Awareness&trade; badge. When you take the time to learn about identity theft, you bring you and your finances closer to identity protection.
			</p>
			<div class="row">
				<div class="columns medium-6 dash-right">

				<div class="left-side row">
					<div class="columns large-4">
						<?= Asset::img('badges/1.jpg', array('class'=>'badge'));?>
					</div>
					<div class="columns large-8 social-share">
						<h4>Share your badge</h4>
						<ul class="">
							<li><a href="#" target="_blank" class='social-small fb' data-action="fb"></a></li>
							<li><a href="#" target="_blank" class='social-small tw' data-action="tw"></a></li>
							<li><a href="#" target="_blank" class='social-small gp' data-action="gp"></a></li>
							<li><a href="#" target="_blank" class='social-small tu' data-action="tu"></a></li>
						</ul>
					</div>
				</div>

				</div>
				<div class="columns medium-6">
					<div class="right-side row">
						<div class="columns large-6">
							<a href="#" id="btn-results" class="button continue tiny no-bot blue-over"><span>View Results</span></a>
							<p>of your game</p>
						</div>
						<div class="columns large-6">
							<a href="https://secure.lifelock.com/enrollment" id="btn-final-try" target="_blank" class="button continue success tiny no-bot blue-over"><span>Try Lifelock</span></a>
							<p>Free for 30 Days*</p>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="foot">
			<div id="play-again" class="inner">
				<span class='left-medium'><a href="#" class="button continue tiny blue-over"><span>Play Again</span></a></span>
				<p>play again tag</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="columns large-10 large-centered">
			<div class='row video'>
				<div class="columns medium-5 medium-offset-1">
					<h3 class="white alt lower">Learn More About LifeLock<sup>&trade;</sup></h3>
					<p>Comprehensive identity theft protection from LifeLock helps safeguard your finances, credit, and good name. Discover the ease and peace of mind that LifeLock can bring.</p>
				</div>
				<div class="columns medium-6">
					<?= Asset::img('elements/video.jpg');?>
				</div>
			</div>
		</div>
	</div>
	
</div>
</script>