<?= View::forge('header'); ?>
<?= (isset($aboveFold) ? $aboveFold : ""); ?>
	<div class="row">
		<div class="columns large-12">
			<?= $body; ?>
		</div>
	</div>
<?= View::forge('footer'); ?>