<?
$profile = Cache::get("users.$user_email.profile");
$user_q = Cache::get("users.$user_email.questions.$defkey");
function getval(&$question, $key){
	if($question && $key && array_key_exists($key, $question)){
		switch ($key) {
			case 'image_set':
				$set = "";
				foreach ($question[$key] as $img) {
					$set .= "<p><img src='".$img."'></p>";
				}
				return $set;
				break;
			
			case 'image':
				return "<p><img src='".$question['image']."'></p>";
			default:
				return $question[$key];
				break;
		}
		
	}
	return "";
}
?>
</div></div>

<style>
	tr.failed{
		background-color: #f3e4d6 !important;
	}
</style>

<h1>Questions</h1>
<table>
	<?php foreach ($user_q['questions'] as $question):?>
		<? if($question):?>
		<tr class='<?=$question['passed']?'passed':'failed';?>'>
			<td><?= $question['id'];?></td>
			<td><?=getval($question, 'layout');?></td>
			<td><?=getval($question, 'type');?></td>
			<td><?=getval($question, 'explain');?></td>
			<td><?=getval($question, 'text');?></td>
			
			<td><?=getval($question, 'social');?></td>
			<td width="100"><?=getval($question, 'image');?></td>
			<td width="100"><?=getval($question, 'image_set');?></td>
			
		</tr>
		<? endif; ?>
	<?php endforeach ?>
</table>