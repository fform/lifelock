<?
$profile = Cache::get("users.$user_email.profile");
?>
</div></div>

<style>
	td{
		vertical-align: top;
	}
</style>
<h1 class="white">Profile</h1>

<table width="100%">
<tr>
<td width="50%">

<h2 >Facebook</h2>
<table width="100%">
<?php foreach ($profile['fb'] as $in => $val): ?>
	<tr>
		<td><?=$in;?></td>
		<td width="80%"><textarea rows='10'><?=json_encode($val,JSON_PRETTY_PRINT);?></textarea></td>
	</tr>
<?php endforeach ?>
</table>

</td>
<td width="50%">

<h2 >Full Contact</h2>
<table width="100%">
<?php foreach ($profile['fc'] as $in => $val): ?>
	<tr>
		<td><?=$in;?></td>
		<td width="80%"><textarea rows='10'><?=json_encode($val,JSON_PRETTY_PRINT);?></textarea></td>
	</tr>
<?php endforeach ?>
</table>

</td>
</tr>
</table>