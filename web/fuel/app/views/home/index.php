<div id="app">

	<div id="bg" class='fitter'>
		<?= Asset::img('bg/home.jpg', array('class'=>'fit')); ?>
	</div>
	<div id="progress" data-val="5">
		<?= Asset::img('elements/progress.png'); ?>
	</div>
	<div id="home">
		<div class="border">
		<div class="inside">
			<div class="roller" data-roll="up">
			<?= Asset::img('elements/tmi-logo.png',array('class'=>'item')); ?>
			<?= Asset::img('elements/roll-top.png',array('class'=>'rolls up', 'id'=>'rolls-up')); ?>
			</div>
			<p>
				Play the TMI Game to find out how<br>what you do and say online can help<br>a thief steal your identity.
			</p>
			<div class="roller" data-roll="down">
			<a href="<?=$fb_url;?>" class="play-btn item"></a>
			<?= Asset::img('elements/roll-bot.png',array('class'=>'rolls down', 'id'=>'rolls-down')); ?>
			</div>
			
			
		</div>
		</div>
	</div>
	<div id="warning">
		<div class="arrow"></div>
		<div class="inside">
			<div id="geospace"></div>
			<p>If we can see that, a crook can, too.</p>
		</div>
	</div>
	
</div>
<div id="home-below" class='fitter'>
	
	<?= Asset::img('elements/lifelock-tag.png', array('class'=>'lifelock-bug')); ?>
	<div id="facts">
		<a href="<?=$fb_url;?>" class="play-btn"></a>
		<div class="fact dir-up dir-left">
			<div class='info'>It’s happening right now.</div>
			<div class="info-more">Once every two seconds someone is a victim of identity theft. *</div>
		</div>
		<div class="fact dir-up dir-right">
			<div class='info'>It’s probably happened to someone you know.</div>
			<div class="info-more">More than 26 million U.S. adults have been a victim of
	Identity theft in the past 12 months. *</div>
		</div>
		<div class="fact dir-down dir-left">
			<div class='info'>It could happen to you.</div>
			<div class="info-more">1 in 3 people that were notified their information was part of a data breach reported they experienced identity theft. *</div>
		</div>
		<div class="fact dir-down dir-right">
			<div class='info'>It’s wasting our money.</div>
			<div class="info-more">Total cost of identity theft in the past 12 months was more than $19 billion. *</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="columns small-10 small-centered text-center">
		<p class='disclaim'>
			* &quot;Identity Theft Tracking Study&quot; by<br>
			Forrester Consulting on behalf of<br>
			LifeLock, April/May 2014.
		</p>
		<p class='legal'>
			<?= Lang::get('site.legal.top'); ?><br>
			<?= Lang::get('site.legal.bottom'); ?>
		</p>
	</div>
</div>
