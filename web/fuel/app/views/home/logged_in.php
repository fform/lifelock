<a href='<?= Uri::base(); ?>task/logout' class='button secondary tiny'>Log Out</a>


<div class="row">
	<div class="columns">
		<hr>
	</div>
</div>
<div class="row">
<div class="columns small-6">
	
	<h5>Debug</h5>
	<h6>Welcome back <?= $user->profile['fb']['first_name']; ?></h6>

	<p><b>Photos:</b></p>
		<? if( $user->getProp('fb.picture.data')): ?>
		<img src="<?= $user->getProp('fb.picture.data.url');?>" alt="Hi there">
		<? endif;?>
		<? if( $user->getProp('fb.cover.source')): ?>
		<img src="<?= $user->getProp('fb.cover.source');?>" alt="Hi there">
		<? endif;?>
	<? if( $user->getProp('fc.photos')): ?>
	<? foreach($user->profile['fc']['photos'] as $photo) :?>
		<img src="<?= $photo['url'];?>">
	<? endforeach; ?>
	<? endif; ?>
	
	<? if( $user->getProp('fb.photos.data')): ?>
	<h4>FB Photos</h4>
	<? foreach($user->getProp('fb.photos.data') as $photo) :?>
		<img src="<?= $photo['source'];?>">
	<? endforeach; ?>
	<? endif; ?>
</div>
<div class="columns small-6">
	<dl>
		<dt>Birthdate</dt>
		<dd><?= $user->getProp('fb.birthday', 'The Past');?></dd>
		<dt>Location</dt>
		<dd><?= $user->getProp('fb.location.name', 'Somewhere');?></dd>
		<? if($user->getProp('fb.significant_other.name')): ?>
		<dt>Better Half</dt>
		<dd><?= $user->getProp('fb.significant_other.name');?></dd>
		<? endif; ?>
	</dl>

	<? if($user->getProp('fb.tagged_places.data')): ?>
	<p><b>Places:</b></p>
	<ul>
	<? foreach($user->getProp('fb.tagged_places.data') as $location) :
		$loc = $location['place']['location'];
		$city = array_key_exists('city', $loc) ? $loc['city'] : "";
		$state = array_key_exists('state', $loc) ? $loc['state'] : "";
	?>
		<? if(empty($city)) :?>
		<li><?= $location['place']['name'];?></li>
		<? else: ?>
		<li><?= $location['place']['name'];?> in <?= $city;?>, <?= $state;?></li>
		<? endif; ?>
	<? endforeach; ?>
	</ul>
	<? endif; ?>

	<? if($user->getProp('fc.contactInfo.websites')): ?>
	<p><b>Websites:</b></p>
	<ul>
	<? foreach($user->profile['fc']['contactInfo']['websites'] as $website) :?>
		<li><a href="<?= $website['url'];?>"><?= $website['url'];?></a></li>
	<? endforeach; ?>
	</ul>
	<? endif; ?>

	<? if($user->getProp('fb.links.data')): ?>
	<p><b>Links:</b></p>
	<ul>
	<? foreach($user->getProp('fb.links.data') as $link) :?>
		<li><a href="<?= $link['link'];?>"><?= $link['name'];?></a></li>
	<? endforeach; ?>
	</ul>
	<? endif; ?>

	<? if($user->getProp('fb.likes.data')): ?>
	<p><b>Likes:</b></p>
	<ul>
	<? foreach($user->getProp('fb.likes.data') as $link) :?>
		<li><?= $link['name'];?></li>
	<? endforeach; ?>
	</ul>
	<? endif; ?>

	<? if($user->getProp('fc.socialProfiles')): ?>
	<p><b>Social:</b></p>
	<ul>
	<? foreach($user->profile['fc']['socialProfiles'] as $social) :?>
		<li><a href="<?= $social['url'];?>"><?= $social['typeName'];?></a></li>
	<? endforeach; ?>
	</ul>
	<? endif; ?>

	<? if($user->getProp('fc.organizations')): ?>
	<p><b>Companies:</b></p>
	<ul>
	<? foreach($user->profile['fc']['organizations'] as $companies) :?>
		<li><?= $companies['name'];?></li>
	<? endforeach; ?>
	</ul>
	<? endif; ?>
</div>
</div>
<hr>
<b>FB Data</b>
<textarea name="fb" id="fb" cols="30" rows="20">
	<?= json_encode( $user->profile['fb'], JSON_PRETTY_PRINT); ?>
</textarea>
<hr>
<b>Full Contact Data</b>
<textarea name="fb" id="fb" cols="30" rows="20">
	<?= json_encode( $user->profile['fc'], JSON_PRETTY_PRINT); ?>
</textarea>