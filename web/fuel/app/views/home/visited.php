<table>

</table>
<br><br><br>
<div id="visit-list">Look like some of your favorites <span></span></div>
<script>
	var links = {
		'Google': 'http://google.com/',
		'Google https': 'https://www.google.com/',
		'This': 'http://lifelock.dev/home/visited',
		'Mozilla': 'http://blog.mozilla.org/security/2010/03/31/plugging-the-css-history-leak/',
		'Buzzfeed': 'http://www.buzzfeed.com/',
		'The Verge': 'http://www.theverge.com/',
		'Yahoo': 'https://www.yahoo.com/',
		'Mint': 'https://www.mint.com/',
		'Twitter': 'https://twitter.com/',
		'Google News': 'https://news.google.com/'
	};
document.addEventListener( "DOMContentLoaded", function thing(){
	var link_list = [];
	for(var i in links){
		var link = "<a href='" + links[i] +"' class='vlink'>" + links[i] +"</a>";
		$("table").append("<tr><td>" + i + "</td><td>"+link+"</td></tr>");
		
		link_list.push(link);
	}
	$("#visit-list span").append(link_list.join('  '));
});
</script>