<?
	$users = DB::select()->from('user')->order_by('id','desc')->execute();
?>
<style>
	a{
		color: blue;
	}
</style>
<div class="row shade">
	<div class="columns">
		<h1>Report</h1>
		<table>
			<? foreach( $users as $user ):
				$ekey = md5($user['email']);
				
				$profile = Cache::get("users.$ekey.profile");
				$user_q = Cache::get("users.$ekey.questions.$defkey");
				//Debug::dump($questions);die;
				$total = 0;	
				$valid = 0;
				foreach($user_q['questions'] as $q){
					if($q){
						$total += 1;
						if($q['passed'] == true){
							$valid += 1;
						}
					}
				}
			?>
			<tr>
				<td width="50"><?= $user['id']; ?></td>
				<td><?= $user['email']; ?></td>
				<td><?= $profile['fb']['name']; ?></td>
				<td><?= $valid;?> / <?= $total; ?></td>
				<td><?= sprintf('%0.2f',(100*$valid/$total));?> %</td>
				<td>
					<a href="<?=Uri::base();?>home/report/1/<?=$ekey;?>">Full Report</a>
				</td>
				<td>
					<a href="<?=Uri::base();?>home/report/profile/<?=$ekey;?>">Full Profile</a>
				</td>
			</tr>
			<? endforeach;?>
		</table>
	</div>
</div>