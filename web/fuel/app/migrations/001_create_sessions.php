<?php

namespace Fuel\Migrations;

class Create_sessions
{
	public function up()
	{
		\DBUtil::create_table('sessions', array(
			'session_id' => array('constraint' => 40, 'type' => 'varchar'),
			'previous_id' => array('constraint' => 40, 'type' => 'varchar'),
			'user_agent' => array('type' => 'text'),
			'ip_hash' => array('constant' => 32, 'type' => 'char'),
			'created' => array('constant' => 10, 'type' => 'int', 'unsigned'=>true),
			'updated' => array('constant' => 10, 'type' => 'int', 'unsigned'=>true),
			'payload' => array('type' => 'longtext'),
		), array('session_id'));
		\DBUtil::create_index('sessions', 'previous_id', 'previous_id', 'UNIQUE');
	}

	public function down()
	{
		\DBUtil::drop_table('sessions');
	}
}