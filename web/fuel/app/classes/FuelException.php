<?php
/**
* FuelException Override
*/
class FuelException extends Fuel\Core\FuelException
{
	
	public function __construct($msg="")
	{	
		parent::__construct($msg);
		$type = get_class($this);

		if($type == "Fuel\Core\ConfigException" OR $type == "Fuel\Core\HttpNotFoundException"){
			return $this;
		}
		
		$token = \Config::get('site.rollbar.access_token');
	
		//if(Fuel::$env == Fuel::PRODUCTION AND !empty($token)){
		if(Fuel::$env != Fuel::DEVELOPMENT AND !empty($token)){

			if(!is_null($this)){
				Rollbar::init(array(
					'access_token' => $token,
					'environment' => Fuel::$env,
					'root' => DOCROOT,
				));
				Rollbar::report_exception($this);
			}
		}
		
		return $this;
	}
}