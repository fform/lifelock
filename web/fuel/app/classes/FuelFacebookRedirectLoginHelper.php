<?php

use \Facebook\FacebookRedirectLoginHelper;
use \Facebook\FacebookSDKException;


class FuelFacebookRedirectLoginHelper extends FacebookRedirectLoginHelper{
	protected function storeState($state){
		$this->state = $state;
		Session::set('fb_state', $state);
	}
	protected function loadState(){
		$state = Session::get('fb_state');
		
		if (isset($state)) {
			$this->state = $state;
			return $this->state;
		}
		return null;
	}
}