<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphObject;

/**
* 
*/
function swrap($txt = ""){
	return "<span class='answer'>$txt</span>";
}

function swrap_join($glue, $pieces, $final_glue = ' and '){
	$listing = "";
	$join = $glue;
	$join_final = $final_glue ?: $glue;
	$last = array_shift($pieces);

	$listing = implode($join, array_map("swrap",$pieces)) . $join_final . swrap($last);
	/*
	foreach ($pieces as $el) {
		$listing .= swrap($el) . $join;
	}
	$listing = substr($listing, 0, strlen($listing)-strlen($join));
	*/
	return $listing;
}

function add_update_list(&$list, $user_id, $new_item){
	if($user_id == $new_item['id']){
		return;
	}
	$id = $new_item['id'];
	if(!array_key_exists($id , $list)){
		$list[$id] = array(
			'name'=> $new_item['name'],
			'count'=> 1
		);
	}else{
		$list[$id]['count'] += 1;
	}
}

class Model_Question
{
	static $list;

	static public function wrap($txt=''){
		return swrap($txt);
	}

	static public function load($question, $user = null){
		if(!static::$list){
			$questions = Lang::load('questions');
			static::$list = $questions['list'];
		}
		
		$slice = array_slice( static::$list , (int)$question, 1 );
		$data = current($slice);

		$passed = true;
		if(!$data){
			return null;
		}
		$response = array();
		if(array_key_exists('layout', $data)){
			$response['layout'] = $data['layout'];
		}
		$response['explain'] = array_key_exists('exp', $data) ? $data['exp'] : '';
		$args = array($data['q']);
		if($data['type'] == 'custom'){
			try {
				$result = call_user_func("static::question_" . $data['fnc'], $data, $user );
				$passed = array_key_exists('failed', $result) ? false : true;
				$response = array_merge($response, $result);
			} catch (Exception $e) {
				error_log($e->getMessage());
				$passed = false;
			}
			
			
		}else{
			foreach($data['vars'] as $var){
				try {
					if(is_array($var)){
						$something_passed = array();
						foreach($var as $item){
							$result = $user->getProp($var,null);
							if($result){
								array_push($something_passed, $result);
							}
						}
						if(count($something_passed)){
							array_push($args, swrap($something_passed[0]) );
						}else{
							$passed = false;
						}
					}else{
						$result = $user->getProp($var,null);
						if($result){
							array_push($args, swrap($result) );
						}else{
							$passed = false;
						}
					}

				} catch (Exception $e) {
					$passed = false;
				}
				
				
			}
			try {
				$response['text'] = call_user_func_array('sprintf', $args );
			} catch (Exception $e) {
				$response['text'] = $data['q'];
			}
		}

		if(!$passed){
			return array('text'=>$data['q'], 'passed'=>false);
		}

		$response = array_merge($response, array(
			'type'=>$data['type'] ,
			'social'=>$data['social'],
			'passed'=> $passed
		));
		return $response;
	}

	static private function get_facebook_session(){
		FacebookSession::setDefaultApplication(Config::get('site.fb_app_id'),Config::get('site.fb_app_secret'));
		$access_token = Session::get('fb_token');
		$session = new FacebookSession($access_token);
		return $session;
	}

	static private function question_weather( $question, $user = null ){
		$location = $user->getProp('fb.location.name');
		$geo = $user->getProp('geo');
		$url = sprintf('https://api.forecast.io/forecast/%s/%s,%s', Config::get('site.forecastio_api'), $geo->location->latitude, $geo->location->longitude );
		$curl = Request::forge($url, 'curl');
		$weather = json_decode($curl->execute());

		$response = sprintf($question['q'], swrap($location), swrap($weather->currently->summary) );

		return array('text'=> $response );
	}

	static private function question_language( $question, $user = null ){
		$language = $user->getProp('fb.languages.name');
		if(!$language){
			return array('failed'=>true);
		}
		$translated = $question['language'][ strtolower($language) ];
		if(array_search($language, $question['rtl']) != false){
			$translated = "<span dir='rtl'>$translated</span>";
		}
		$response = swrap($translated) . sprintf($question['q'], swrap($language) );
		return array('text'=> $response );
	}

	static private function question_family_member( $question, $user = null ){
		$session = static::get_facebook_session();
		$family = $user->getProp('fb.family.data');
		if(!$family){
			return array('failed'=>true, 'text'=>$family);
		}
		$passed = false;
		foreach($family as $member){
			if($member['relationship'] == $question['search']){
				$passed = $member;
			}
		}
		if($passed){
			$mother = (new FacebookRequest($session, 'GET', '/'. $passed['id'].'?fields=picture.width(140).height(140),last_name'))->execute()->getGraphObject(GraphObject::className());
			
			$response = sprintf($question['q'], swrap($passed['name']) );
			$picture = $mother->getProperty('picture')->getProperty('url');
			return array( 'text'=> $response, 'image'=> $picture);
		}
		return array('failed'=>true, 'text'=>$family);
	}


	static private function question_status( $question, $user = null ){
		$posts = $user->getProp('fb.posts');
		if(!$posts){
			return array('failed'=>true);
		}
		foreach($posts['data'] as $post){
			if($post['type'] == 'status'){
				$message = swrap( (array_key_exists('message', $post) ? $post['message']:$post['story'] ) );
				$response = sprintf($question['q'], $message );
				return array( 'text'=> $response );
			}
		}
		$response = $question['language'][ strtolower($language) ] . sprintf($question['q'], swrap($language) );
		return array( 'failed'=>true );
	}

	static private function question_image( $question, $user = null ){
		$picture = $user->getProp( $question['vars'][0] );
		if(!$picture){
			return array('failed'=>true);
		}
		
		$url = is_array($picture)? $picture['data']['url'] : $picture;
		
		return array( 'text'=>$question['q'], 'image'=> $url  );
	}

	static private function question_photoset( $question, $user = null ){
		$photos = $user->getProp('fb.photos.data');
		if(!$photos){
			return array('failed'=>true);
		}
		$photoset = array();

		foreach($photos as $photo){
			//array_push($photoset, $photo['source']); // full size
			if(count($photoset) < 4){
				array_push($photoset, $photo['picture']); // thumbs
			}
		}
		
		return array( 'text'=>$question['q'],'image_set'=> array_slice($photoset,0,5)  );
	}

	static private function question_twitter( $question, $user = null ){
		$profiles = $user->getProp('fc.socialProfiles');
		$found = false;
		foreach($profiles as $profile){
			if($profile['type'] == 'twitter'){
				$found = $profile;
			}
		}
		if(!$found){
			return array('failed'=>true);
		}
		$twitter_user = $found['username'];
		$settings = array(
			'oauth_access_token' => Config::get('site.twitter.access'),
			'oauth_access_token_secret' => Config::get('site.twitter.secret'),
			'consumer_key' => Config::get('site.twitter.api_key'),
			'consumer_secret' => Config::get('site.twitter.api_secret')
		);
		$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
		$requestMethod = 'GET';
		$twitter = new TwitterAPIExchange($settings);
		$response = json_decode( $twitter->setGetfield("?screen_name=$twitter_user&count=1")->buildOauth($url, 'GET')->performRequest() , true);
		$tweet = array_shift( $response );
		$text = sprintf($question['q'], swrap( $tweet['text'] ) );
		return array( 'text'=>$text );
	}

	static private function question_birthday( $question, $user = null ){
		//You were born on  %s. Are [insert Astrological sign] as [insert Quality related to their sign] as people say?
		$birthday = $user->getProp('fb.birthday');

		$parts = explode('/', $birthday);
		$month = (int)$parts[0];
		$day = (int)$parts[1];

		$amt = 8 + (100*$month) + $day;
		$sign_name = "";
		$sign_quality = "";

		foreach($question['signs'] as $sk=>$sign){
			$sparts = explode('_', $sk);
			$first = (int) $sparts[0];
			$last = (int) $sparts[1];
			if($amt >= $first AND $amt <= $last){
				$sign_name = $sign['name'];
				$sign_quality = $sign['quality'];
			}
		}
		$text = sprintf($question['q'], swrap( $birthday ), swrap( $sign_name ) );

		return array('text'=>$text);
	}

	static private function question_places( $question, $user = null ){
		$places = $user->getProp('fb.tagged_places.data');
		$list = array();

		foreach($places as $place){
			if(count($list) < 4){
				array_push($list, $place['place']['name']);
			}
			
		}
		$output = swrap_join(', ', $list) . ".";
		$text = sprintf($question['q'], $output);
		return array('text'=>$text);
	}

	static private function question_significant_other( $question, $user = null ){
		$genders = array('male'=>'Him', 'female'=>'Her');
		$other = $user->getProp('fb.significant_other');
		$session = static::get_facebook_session();
		$graph = (new FacebookRequest($session, 'GET', '/'. $other['id'].'?fields=picture.width(140).height(140),gender'))->execute()->getGraphObject(GraphObject::className());
		$other_name = $other['name'];
		$other_gender = $graph->getProperty('gender');
		$other_picture = $graph->getProperty('picture')->getProperty('url');
		$title = (array_key_exists($other_gender, $genders) ? swrap($genders[$other_gender]) : "they");
		$text = sprintf($question['q'], swrap( $other_name ), $title );
		return array('text'=>$text, 'image'=>$other_picture);
	}

	static private function question_matching_name( $question, $user = null ){
		$last_name = $user->getProp('fb.last_name');
		$friends = $user->getProp('fb.friends.data');
		$found = null;
		$result = array();
		foreach($friends as $friend){
			preg_match('/'.$last_name.'/i', $friend['name'], $compare);
			if($compare){
				$found = $friend;
				$result['text'] = sprintf($question['q'], swrap( $friend['name'] ) );
				$result['image'] = $friend['picture']['data']['url'];
				return $result;
			}
		}
		
		return array('failed'=>true);
	}

	static private function question_event( $question, $user = null ){
		$events = $user->getProp('fb.events.data');
		
		foreach($events as $event){
			$date = Date::create_from_string($event['start_time'], '%Y-%m-%dT%H:%M:%S%z');
			if($event['rsvp_status'] == "attending"){
				if(!$question['in_past'] AND $date->get_timestamp() > time()
					OR $question['in_past'] AND $date->get_timestamp() < time()){
					return array('text'=> sprintf($question['q'], swrap( $event['name'] )) );
				}
			}
		}
		
		return array('failed'=>true);
	}

	static private function question_count( $question, $user = null ){
		$count = count($user->getProp( $question['vars'][0] ));
		if($count){
			return array('text'=> sprintf($question['q'], swrap( $count )));
		}
		
		return array('failed'=>true);
	}

	static private function question_first_match( $question, $user = null ){
		$likes = $user->getProp($question['list']);
		foreach($likes as $like){
			preg_match($question['match'], $like['name'], $compare);
			if($compare){
				return array('text'=> sprintf($question['q'], swrap( $like['name'] )));
			}
		}
		
		return array('failed'=>true);
	}

	static private function question_bank_search( $question, $user = null ){
		$likes = $user->getProp('fb.likes.data');
		$banks = ["179590995428478","11465191261","152431441489088","152789358067261","313163945066","224986157558341","131080263606125","132950960136736","164640996893077","160995133980392","167890209688","6185812851","151901268159778","59062845587","216080375068757","416289528393004","7239334663","366007463426337","131677553516197","186266941387011","381727751890764","143790482303586","140499052681592","309220109224521","21493231379","115816835104562","79747957892","375005220850","44825829365","9132102354"];
		foreach($likes as $like){
			if(array_search($like['id'], $banks) !== false){
				return array('text'=> sprintf($question['q'], swrap( $like['name'] )));
			}
		}
		
		return array('failed'=>true);
	}

	static private function question_photos_of_friends( $question, $user = null ){
		$user_id = $user->getProp('fb.id');
		$photos = $user->getProp('fb.photos.data');
		$images = array();
		$names = array();
		foreach($photos as $photo){
			$use_photo = null;
			foreach($photo['tags']['data'] as $tag){
				if($tag['id'] != $user_id AND array_search($tag['name'], $names) === false){
					$use_photo = $tag['name'];
					break;
				}
			}
			if(!is_null($use_photo)){
				array_push($images, $photo['picture']);
				array_push($names, $use_photo);
			}
			if(count($images) >= 4){
				$list = swrap_join(' and ', $names);
				$text = sprintf($question['q'], $list );
				return array('text'=>$text, 'image_set'=> $images);
			}
		}
		return array('failed'=>true);
	}

	static private function question_a_is_for( $question, $user = null ){
		$friends = $user->getProp('fb.friends.data');
		$images = array();
		$names = array();
		foreach($friends as $friend){
			if(strtolower($friend['name'][0]) == 'a' AND $friend['picture']['data']['is_silhouette'] == false){
				array_push($images, $friend['picture']['data']['url']);
				array_push($names, $friend['name']);
			}
			if(count($names) >= 5){
				break;
			}
		}

		if(count($images)){
			$name_list = swrap_join(' and ', $names) . ".";
			$text = sprintf($question['q'], $name_list);
			return array('text'=>$text, 'image_set'=> $images);
		}

		return array('failed'=>true);
	}



	static private function question_most_frequent( $question, $user = null ){
		$user_id = $user->getProp('fb.id');
		$stories = $user->getProp('fb.posts.data');
		$tagged = $user->getProp('fb.tagged.data');
		$list = array();
		foreach($stories as $story){
			foreach($story['story_tags'] as $tag){
				add_update_list($list, $user_id, array_pop($tag) );
			}
			if(array_key_exists('likes', $story)){
				foreach($story['likes']['data'] as $story_like){
					add_update_list($list, $user_id, $story_like );
				}
			}
			if(array_key_exists('comments', $story)){
				foreach($story['comments']['data'] as $comment){
					add_update_list($list, $user_id, $comment['from'] );
				}
			}
			
		}
		foreach($tagged as $tag_item){
			add_update_list($list, $user_id, $tag_item['from'] );
		}

		$found = null;
		$most = 0;
		foreach($list as $user){
			if($user['count'] > $most){
				$found = $user;
				$most = $user['count'];
			}
		}
		if(!is_null($found)){
			$text = sprintf($question['q'], swrap( $found['name'] ) );
			return array('text'=>$text);
		}

		return array('failed'=>true);
	}
}