<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphObject;
use Facebook\FacebookRequestException;
use \FuelFacebookRedirectLoginHelper;
use GeoIp2\Database\Reader;

class Model_User extends \Model_Crud
{
	protected static $_table_name = 'user';

	static public function geo_location(){
		$reader = new Reader(APPPATH ."cache/GeoLite2-City.mmdb");
		$real_ip = Input::real_ip();
		if($real_ip == "127.0.0.1" AND Fuel::$env == Fuel::DEVELOPMENT){
			$real_ip = "66.214.133.34";
		}
		$record = $reader->city($real_ip);
		return $record;
	}

	static public function init_with_fb_session($sess){

		$user_email_lookup = (new FacebookRequest($sess, 'GET', '/me?fields=id,email'))->execute()->getGraphObject(GraphObject::className());
		$user_email = $user_email_lookup->getProperty('email');
		$user_lookup = static::find_one_by_email($user_email);
		Session::set('email', $user_email);

		if($user_lookup){
			//Found user in DB, already have dump
			return $user_lookup;
		}

		

		//Get entire FB dump of user
		//excluding: feed
		//music.limit(5)
		$everything = "/me?fields=email,about,accounts,achievements,activities,address,age_range,albums,bio,birthday,books,context,cover,currency,devices,education,events,family,favorite_athletes,favorite_teams,first_name,friendlists,friends{picture.width(140).height(140),name},games,gender,groups,hometown,id,inspirational_people,install_type,installed,interested_in,interests,is_verified,languages,last_name,likes,link,links,location,meeting_for,middle_name,movies,music,name,name_format,photos,picture.width(140).height(140),political,posts.limit(5),quotes,relationship_status,religion,scores,security_settings,significant_other,sports,tagged,tagged_places,television,test_group,third_party_id,timezone,updated_time,verified,video_upload_limits,videos,viewer_can_send_gift,website,work";
		$user_profile = (new FacebookRequest($sess, 'GET', $everything))->execute()->getGraphObject(GraphObject::className());
		
		//Get Full Contact person lookup
		$full_contact = new Services_FullContact_Person( Config::get('site.fullcontact_api') );
		$result = $full_contact->lookupByEmail($user_email);

		//Sanitize results and rip out nested classes
		$profile = array(
			'fb'=> json_decode(json_encode($user_profile->asArray()), true),
			'fc'=> json_decode(json_encode( (array) $full_contact->response_obj ), true)
		);

		$friends = (new FacebookRequest($sess, 'GET', '/me/taggable_friends?fields=picture.width(100).height(100),first_name,name,last_name,id'))->execute()->getGraphObject(GraphObject::className());
		$profile['fb']['friends'] = json_decode(json_encode($friends->asArray()), true);

		$user = static::forge(array(
			'email'=> $user_email,
			'profile'=> $profile
		));
		
		$user->save();
		

		return $user;
	}

	public function getProp($path, $default=null){
		$parts = explode('.', $path);

		if($parts[0] == "geo"){
			$record = static::geo_location();
			
			if(count($parts) == 1){
				return $record;
			}

			switch($parts[1]){
				case 'city': $result = $record->city->name; break;
				default: $result = $record;
			}

			return $result;
		}

		if($parts[0] == "agent"){
			switch($parts[1]){
				case "browser": return Agent::browser(); break;
				case "os": return Agent::platform(); break;
			}
			return "Something";
		}

		$prev = $this->profile;
		$i = 1;
		$total = count($parts);
		foreach($parts as $part){
			$how = null;
			if(strpos($part, ':') !== false){
				list($part, $how) = explode(':', $part);
			}
			if(array_key_exists($part, $prev)){
				if(is_null($prev[$part])){
					return null;
				}
				$is_assoc = is_array($prev[$part]) && (bool)count(array_filter(array_keys($prev[$part]), 'is_string'));
				if(!$is_assoc && $i < $total){
					switch ($how) {
						case 'random':
							$index = array_rand($prev[$part]);
							$prev = $prev[$part][$index];
							break;
						case 'last':
							$prev = array_pop($prev[$part]);
							break;
						default:
							//first
							$prev = array_shift($prev[$part]);
							break;
					}
					
				}else{
					$prev = $prev[$part];
				}
				
			}else{
				return $default;
			}

			$i++;
		}
		return $prev;
	}

	protected static function post_find($result){

		if ($result !== null){
			foreach($result as $row){
				$filename = md5($row['email']) . '.json';
				try {
					$row['profile'] = Cache::get('users.'. md5($row['email']) .'.profile');
					//$profile = File::read(USER_RECORDS . $filename, true);
					//$row['profile'] = json_decode($profile, true);
				} catch (Exception $e) {
					
					
					$row['profile'] = array('fb'=>null,'fc'=>null);
				}
			}
		}

		return $result;
	}

	protected function pre_delete(&$query){
		// try {
		// 	File::delete( USER_RECORDS . md5($this->email) . '.json' );
		// } catch (\Fuel\Core\InvalidPathException $e) {
		// 	
		// }
		
		Cache::delete_all("users." . md5($this->email) . ".questions");
		Cache::delete_all("users." . md5($this->email));
	}

	protected function prep_values($data){
		
		
		Cache::set("users." . md5($this->email) . ".profile", $data['profile']);
		//File::update(USER_RECORDS, md5($data['email']) . '.json', json_encode($data['profile']));
		unset($data['profile']);

		return $data;
	}
}