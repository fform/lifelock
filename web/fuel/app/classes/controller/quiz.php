<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphObject;
use Facebook\FacebookRequestException;
use \FuelFacebookRedirectLoginHelper;

class Controller_Quiz extends Controller_Base
{
	public function action_index()
	{	
		$access_token = Session::get('fb_token');
		if ($access_token) {
			$quiz_view = View::forge('quiz/index',array(
				
			));
			return Response::forge(
				View::forge('none', array('body'=> $quiz_view))
			);
		} else {
			return Response::redirect('/');
		}
	}

	public function action_404()
	{
		return Response::forge(View::forge('404'), 404);
	}

}
