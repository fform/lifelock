<?php
/**
* Base Controller
*/

use Facebook\FacebookSession;

class Controller_Base extends Controller
{
	public function before(){
		FacebookSession::setDefaultApplication(Config::get('site.fb_app_id'),Config::get('site.fb_app_secret'));
	}

	static public function make_rest($data, $responseCode = 200){
		$response = \Response::forge(null, $responseCode);
		$response->set_header('Content-Type', 'application/json');
		$response->body = Format::forge($data)->to_json();
		return $response;
	}
}