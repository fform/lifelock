<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use \FuelFacebookRedirectLoginHelper;

class Controller_Fb extends Controller_Base{
	public function action_callback(){
		$session = null;
		//echo "code:" . isset($_GET['code']) ? $_GET['code'] : null;
		//echo ""
		try {
			$helper = new FuelFacebookRedirectLoginHelper( Config::get('site.fb_callback') );
			$session = $helper->getSessionFromRedirect();
			
		} catch(FacebookRequestException $ex) {
		// When Facebook returns an error
			error_log( "fb error ". $ex->getMessage());
		} catch(SessionException $fsex){
			error_log( "Session exception:" . $fsex->getMessage());
		} catch(FacebookSessionException $fsex){
			error_log( "Session exception:" . $fsex->getMessage());
		} catch(\Exception $ex) {
		// When validation fails or other local issues
			error_log( "valida error ");
			error_log( $ex->getMessage());
		}
		if ( $session) {
		// Logged in.
			//error_log( "logged in: ". $session->getAccessToken());
			Session::set('fb_token', $session->getAccessToken());
			return Response::redirect('/');
			//Debug::dump($session);
		}else{
			error_log( "Couldn't login");
			//Debug::dump($session);
		}
	}

	public function action_getstuff(){
		$access_token = Session::get('fb_token');
		$session = new FacebookSession($access_token);
		if($session) {

		  try {

		    $user_profile = (new FacebookRequest(
		      $session, 'GET', '/me?fields=id,email,first_name,gender,languages,last_name,link,location,name,timezone,updated_time,address,bio,birthday,cover,education,favorite_athletes,favorite_teams,hometown,devices,religion,sports,significant_other,website,activities'
		    ))->execute()->getGraphObject(GraphUser::className());

		   	//Debug::dump($user_profile);

		  } catch(FacebookRequestException $e) {

		    echo "Exception occured, code: " . $e->getCode();
		    echo " with message: " . $e->getMessage();

		  }   

		}
	}
}