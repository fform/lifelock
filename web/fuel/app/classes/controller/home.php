<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphObject;
use Facebook\FacebookRequestException;
use \FuelFacebookRedirectLoginHelper;

class Controller_Home extends Controller_Base
{

	public function action_index()
	{	
		$access_token = Session::get('fb_token');
		if ($access_token) {
			return Response::redirect('/quiz');
		} else {
			$helper = new FuelFacebookRedirectLoginHelper( Config::get('site.fb_callback') );
			$loginUrl = $helper->getLoginUrl(array(
				"user_about_me","user_actions.books","user_actions.music","user_actions.news","user_actions.video","user_activities","user_birthday","user_education_history","user_events","user_friends","user_games_activity","user_groups","user_hometown","user_interests","user_likes","user_location","user_photos","user_relationship_details","user_relationships","user_religion_politics","user_status","user_tagged_places","user_videos","user_website","user_work_history","email","read_friendlists","read_insights","read_mailbox","read_page_mailboxes","read_stream","rsvp_event"
			));
			return Response::forge(View::forge('none', array('body'=> View::forge('home/index',array(
				'fb_url'=> $loginUrl
			)))));
		}
		
	}

	public function action_test(){
		$access_token = Session::get('fb_token');
		if ($access_token) {
			$session = new FacebookSession($access_token);
			$user = Model_User::init_with_fb_session($session);
			
			return Response::forge(View::forge('layout', array('body'=> 
				View::forge('home/logged_in',array(
					'message'=> "Welcome Back " . $user->email,
					'user'=> $user
				)
			))));
		}

		return Response::redirect('/');
	}

	public function action_visited(){
		return Response::forge(View::forge('layout', array('body'=> 
			View::forge('home/visited',array(
			)
		))));
	}

	public function action_report($type=null, $user_email=null){
		if(Input::post('pw') && Input::post('pw') == "qbkRL5v62WJM16L5gaxu"){
			Session::set('report_authed', true);
		}
		if(!Session::get('report_authed')){
			return Response::forge(View::forge('layout', array('body'=> View::forge('home/report_auth') ) ) );
		}

		$report = 'report';
		if($type){
			$report = 'report_full';
			if($type == "profile"){
				$report = 'report_profile';
			}
		}
		return Response::forge(View::forge('layout', array(
			'body'=> View::forge('home/'. $report,array(
				'user_email'=>$user_email,
				'defkey'=> md5("500") // total . offset
			))
		)));
	}

	public function action_404()
	{
		return Response::forge(View::forge('404'), 404);
	}

}
