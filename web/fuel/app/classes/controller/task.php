<?php

use Facebook\FacebookSession;
use Facebook\Entities\AccessToken;

class Controller_Task extends Controller_Base{
	public function action_index(){

	}

	public function action_logout(){
		$email = Session::get('email');
		
		if($email){
			$user = Model_User::find_one_by_email($email);
			$user->delete();
		}
		
		Session::destroy();
		return Response::redirect('/');
	}

	public function action_hijack(){
		if(Fuel::$env != Fuel::DEVELOPMENT){
			return;
		}
		$fb_state = "5224671e244584aac242c1fcfee40f64";
		$fb_token = new AccessToken("CAAEnpV2mdVsBACFh2pmSERZC8BeS3vI7bradsZAKufaNzrXhZBDRlFMFz18MaFXHTWyfmT39dN2rljOxPDEZC6Wc9HBwSn8rehXX3lMUVMov0czZCBsPiNkILLSnq0GtEFvefG4HJ20qXX0xm7XAYIicEveb0U7CuLFoxSQ0mnEH3kZAeZCZACIp064ofshxwUoZA4tQQ7fQyjqLGzXOA1tIZB");
		Session::set('fb_state', $fb_state);
		Session::set('fb_token', $fb_token);
		//Debug::dump($fb_token);
		return Response::redirect('/');
	}
}