<?php

use Facebook\FacebookSession;

class Controller_Api extends Controller_Rest
{
	static $user;

	public function before(){
		parent::before();
		// $http = "http" . (preg_match("/https/", Input::referrer()) ? "s" : "") . "://";
		// $this->response->set_header('Access-Control-Allow-Origin', $http.'creative-wall-graphics.myshopify.com');
		// $this->response->set_header('Access-Control-Allow-Headers', 'X-Requested-With, X-Prototype-Version, Content-Disposition, Cache-Control, Content-Type');
		// $this->response->set_header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE');
		// $this->response->set_header('Access-Control-Allow-Credentials', 'true');
		FacebookSession::setDefaultApplication(Config::get('site.fb_app_id'),Config::get('site.fb_app_secret'));
	}

	public function get_all(){
		return $this->response( Session::get() );
	}

	public function get_teaser(){
		$questions = Lang::load('teasers');
		$list = $questions['list'];
		$result = array();
		$loc = Model_User::geo_location();

		foreach($list as $q){
			switch($q['type']){
				case "weather":
					$url = sprintf('https://api.forecast.io/forecast/%s/%s,%s', Config::get('site.forecastio_api'), $loc->location->latitude, $loc->location->longitude );
					$curl = Request::forge($url, 'curl');
					$weather = json_decode($curl->execute());
					$response = sprintf($q['q'], Model_Question::wrap($weather->currently->summary), Model_Question::wrap($loc->city->name) );
					array_push($result, $response);
				break;
				case "location":
					$txt = sprintf($q['q'], Model_Question::wrap($loc->city->name));
					array_push($result, $txt);
				break;
				case "agent":
					$txt = sprintf($q['q'], Model_Question::wrap(Agent::browser()), Model_Question::wrap(Agent::platform()));
					array_push($result, $txt);
				break;
			}
		}
		return $this->response(array('list'=>$result));
	}

	public function get_index(){
		$email = Session::get('email');
		$total = is_numeric(Input::get('count'))?(int)Input::get('count'):20;
		$offset = is_numeric(Input::get('offset'))?(int)Input::get('offset'):0;


		$cache_path_fmt = "users.%s.questions.%s";
		$cache_path_offset = md5($total . $offset);
		$cache_path = sprintf($cache_path_fmt, md5($email), $cache_path_offset);

		try{
			if(Fuel::$env != Fuel::PRODUCTION AND Input::get('bust')){
				throw new CacheNotFoundException("Bust");
			}
			$content = Cache::get($cache_path);
			
			return $this->response($content);
		} catch (\CacheNotFoundException $e){
			
		}

		if(strpos($email, "@tfbnw.net") !== false){
			error_log('Test FB user:' . $email);
			return $this->response(json_decode(file_get_contents(APPPATH.'/views/fakeuser.json')));
		}

		$access_token = Session::get('fb_token');
		if ($access_token) {
			$session = new FacebookSession($access_token);
			$user = Model_User::init_with_fb_session($session);
		} else {
			return Response::redirect('/');
		}

		if(!$user){
			return $this->response(array("user"=> null), 404);
		}

		

		$questions = array();
		
		for($i=$offset; $i<= ($offset + $total); $i++){
			$result = Model_Question::load($i, $user);
			$question = null;
			if($result){
				$question = array_merge($result, array('id'=>(int)$i));
			}
			
			array_push($questions, $question);
		}

		$result = array(
			'questions'=>$questions,
			'user'=> $user->getProp('fb.first_name')
		);
		if(is_array($questions) AND $user){
			$cache_path = sprintf($cache_path_fmt, md5(Session::get('email')), $cache_path_offset);
			
			Cache::set($cache_path, $result);
		}else{
			
		}
		
		return $this->response($result);
	}
}
