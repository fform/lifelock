<?php

return array(
	
	'auto_initialize'	=> true,

	// if no session type is requested, use the default
	'driver'			=> 'db',

	// check for an IP address match after loading the cookie (optional, default = false)
	'match_ip'			=> false,

	// check for a user agent match after loading the cookie (optional, default = true)
	'match_ua'			=> true,

	// cookie domain  (optional, default = '')
	'cookie_domain' 	=> '',

	// cookie path  (optional, default = '/')
	'cookie_path'		=> '/',

	// if true, the session expires when the browser is closed (optional, default = false)
	'expire_on_close'	=> false,

	// 1 week
	'expiration_time'	=> 604800,

	// session ID rotation time  (optional, default = 300)
	'rotation_time'		=> 300,

	// special configuration settings for cookie based sessions
	'cookie'			=> array(
		'cookie_name'		=> 'lifelock',				// name of the session cookie for cookie based sessions
	),

	array(
	 'cookie_name'		=> 'lifelock',
	 'database'			=> null,
	 'table'				=> 'sessions',
	 'gc_probability' => 5
 )
);


