<?php
return array(
	'version' => 
	array(
		'app' => 
		array(
			'default' => 
			array(
				0 => '001_create_sessions',
				1 => '002_create_users',
				2 => '003_create_user',
			),
		),
		'module' => 
		array(
		),
		'package' => 
		array(
		),
	),
	'folder' => 'migrations/',
	'table' => 'migration',
);
