<?php

return array(
	'driver' => 'gd',

	'presets' => array(
		'mypreset' => array(
			'bgcolor' => '#f00', // Set the background color red
			'filetype' => 'jpg', // Output as jpeg.
			'quality' => 75
		)
	)
);