<?php

namespace Fuel\Tasks;
use Fuel;

class Deploy
{

	public static function run()
	{
		\Cli::write('Cleaning Database...');
		\DB::delete('user')->execute();
		\DB::delete('sessions')->execute();
		
		\Cli::write('Cleaning Cache...');

		$dir_path = APPPATH.'/cache/users';
		if(is_dir($dir_path)){
			\File::delete_dir($dir_path, true, true);	
		}
		
		return \Cli::color('Done', 'green');
	}

}
