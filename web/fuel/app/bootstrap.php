<?php

// Load in the Autoloader
require COREPATH.'classes'.DIRECTORY_SEPARATOR.'autoloader.php';
class_alias('Fuel\\Core\\Autoloader', 'Autoloader');

// Bootstrap the framework DO NOT edit this
require COREPATH.'bootstrap.php';


Autoloader::add_classes(array(
	// Add classes you want to override here
	'FuelException' => APPPATH.'classes/FuelException.php',
));

// Register the autoloader
Autoloader::register();

/**
 * Your environment.  Can be set to any of the following:
 *
 * Fuel::DEVELOPMENT
 * Fuel::TEST
 * Fuel::STAGING
 * Fuel::PRODUCTION
 */
$env = getenv('APP_ENVIRONMENT');
Fuel::$env = ( isset($env) && !empty($env) ? $env:Fuel::DEVELOPMENT );
// Initialize the framework with the config file.
Fuel::init('config.php');
