<?php
return array(
	'list'=> array(
		array(
			'q'=> "Hello. Looks like it's a %s day in %s.",
			'type'=> "weather"
		),
		array(
			'q'=> "Glad you stopped by. How are things in %s?",
			'type'=> "location"
		),
		array(
			'q'=> "We see you're running %s on %s.  Are you certain that's the most secure one?",
			'type'=> "agent"
		)
	)
);