<?php
return array(
	'meta'=> array(
		'title'=> "LifeLock TMI",
		'description'=> "A game of TMI is a lesson in online safety. Play now to see what thieves can find out about you."
	),

	'social-share'=> "A game of TMI is a lesson in online safety. Play now to see what thieves can find out about you.",

	'legal'=> array(
		'top'=> "Copyright &copy; 2014 LifeLock. All Rights Reserved.",
		'bottom'=> "LifeLock and &quot;Relentlessly Protecting Your Identity&quot; are registered trademarks and The LockMan logo is a trademark of LifeLock, Inc."
	)
);