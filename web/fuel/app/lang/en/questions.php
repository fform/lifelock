<?php
return array(
	'list'=> array(
	

	//Welcome Question
	array(
		'q'=> "Hi, %s. Ready to play TMI?",
		'exp'=> "Ready to find out what a thief might already know about you? Each game has 10 questions, and you can play %d time%s.", //# time(s)
		'vars'=> array(
			'fb.first_name'
		),
		'layout'=>"scenario",
		'type'=> "welcome",
		'social'=>""
	),
	array(
		'q'=> "Welcome back, %s. Ready to play again?",
		'exp'=> "There's enough info out there about you for %d more round%s of TMI.", // # more round(s)
		'vars'=> array(
			'fb.first_name'
		),
		'layout'=>"scenario",
		'type'=> "welcome",
		'social'=>""
	),

	// Game Questions
	//1
	array(
		'q'=> "Hi, %s. Welcome to TMI.",
		'exp'=> "Your name can be the entry point for online thieves. Once they have that, there's so much info they can get.",
		'vars'=> array(
			'fb.name'
		),
		'layout'=>"scenario",
		'type'=> "text",
		'social'=>"My name is all over social media, and that's the starting point for online thieves. Is your name safe? Play TMI to get informed."
	),

	//2
	array(
		'q'=> "Love that profile picture. Mind if I borrow it?",
		'exp'=> "Thieves could use your profile pic to create passports or other forms of ID. They can also use it to create fake social profiles that mirror your real ones. ",
		'vars'=> array(
			"fb.picture.data.url"
		),
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "image",
		'social'=>"I never thought about the fact that criminals could use my profile pic to create fake IDs. Which means they can use yours, too. Play TMI to find out how."
	),

	//3
	array(
		'q'=> "I see you attended %s. Do you usually tell the world when your house will be empty for long periods of time?",
		'exp'=> "Your home is your castle, so do all you can to protect it. That means not allowing strangers to figure out when you won't be there.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "event",
		'in_past'=>true,
		'social'=>"Yikes! I thought only my friends could see my RSVPs, but TMI knows when I'm away from home. What do your RSVPs tell a thief? Play TMI and find out."
	),

	//4
	array(
		'q'=> "So your email address is %s. I'm always looking for a good phishing spot. ",
		'exp'=> '"Phishing" is when a thief pretends to be a trusted source in order to get your personal info. Once they know your email address, they know how to engage with you.'
		,
		'vars'=> array(
			'fb.email'
		),
		'layout'=> "scenario",
		'type'=> "text",
		'social'=>"I only give my email address to trusted friends, but somehow TMI got it. Is your email safe? Play TMI to find out."
	),

	//5
	array(
		'q'=> 'So you\'re a fan of %s? You know what they say: you are what you "like."',
		'exp'=> "Your Facebook likes can reveal the answer to a security question, or just give a fraudster ammo that makes it easier to pose as you.",
		'vars'=> array(
			'fb.likes.data:random.name'
		),
		'layout'=> "scenario",
		'type'=> "text",
		'social'=>"I had no idea that online thieves could use what I follow on social media to target me. Are your likes leaving you exposed? Play TMI to find out."
	),

	//6
	array(
		'q'=> "It took me no time at all to figure out that %s is your dad. ",
		'exp'=> "Your father's name is another popular security question for identify verification. You may want to pick an alternate security question when possible, or create a fake one for Dad that you use instead. ",
		'layout'=> "scenario",
		'type'=> "custom",
		'search'=> 'father',
		'fnc'=> "family_member",
		'social'=>"TMI discovered the names of my relatives. Have you ever used a relative's name in a password? Play TMI to find out how easy it is for a crook to find your relatives."
	),

	//7
	array(
		'q'=> "Looks like you went to %s. %s is the greatest of times.",
		'exp'=> "Certain details may help an online thief paint a clearer picture of you and your background, potentially allowing them to more easily rip you off.",
		'layout'=> "scenario",
		'type'=> "text",
		'vars'=> array(
			'fb.education:last.school.name',
			'fb.education:last.type'
		),
		'social'=>"My profile is private, but TMI knows where I went to school. Online thieves could use my alma matter as ammo. Play TMI to find out if your education is exposing you? "
	),

	//8
	array(
		'q'=> "Ah, you're a %s fan, are you? Mind if I buy a couple tickets to their next game on your card?",
		'exp'=> "What you share and like on Facebook could point a thief to the best purchases to make in your name.",
		'layout'=> "scenario",
		'type'=> "text",
		'vars'=> array(
			'fb.favorite_teams.name'
		),
		'social'=>"Until I played TMI, I didn't realize how much what I follow online gives a thief a picture of who I am. Play TMI to find out how a thief could impersonate you."
	),

	//9
	array(
		'q'=> "I see you have a child named %s. Is it true that a parent's greatest weakness is their child?",
		'exp'=> 'A potential crook can use your child\'s name to answer a security question or pose as them for a "phishing" scam.',
		'layout'=> "scenario",
		'type'=> "custom",
		'search'=> 'son',
		'fnc'=> "family_member",
		'social'=>""
	),

	//10
	array(
		'q'=> "A is for %s",
		'exp'=> "If a thief discovers enough about you online, he or she may look to your friends to see if they're as unprotected as you are. Friends don't let friends get fleeced.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "a_is_for",
		'social'=>"Hey friends, TMI knows who you are! I didn't realize online thieves might use my friend list to find new targets. Play TMI to find out."
	),

	//11
	array(
		'q'=> "You recently tagged %s in a post. You just doubled the number of people who can see what you're up to online.",
		'exp'=> "When you're tagged in a status update, you're giving fraudsters access to your schedule, habits, and whereabouts.",
		'layout'=> "scenario",
		'type'=> "text",
		'vars'=> array(
			'fb.tagged.data:random.message'
		),
		'social'=>"TMI knows every person and place I ever tagged. Are your tags telling potential crooks more than you want them to know? Play TMI to find out."
	),

	//12
	array(
		'q'=> "It's easy to figure out that %s is your mom -- and it's not because you have her eyes.",
		'exp'=> '"What\'s your mother\'s maiden name?" is a common security question. It\'s also an easy one to answer thanks to our social media profiles. Whenever possible, pick an alternate security question.',
		'layout'=> "scenario",
		'type'=> "custom",
		'search'=> 'mother',
		'fnc'=> "family_member",
		'social'=>"TMI discovered the names of my relatives. Have you ever used a relative's name in a password? Play TMI to find out how easy it is for a crook to find your relatives."
	),

	//13
	array(
		'q'=> '"Calling all fans of %s on Facebook! Click here to submit your credit card info and get a FREE item!"',
		'exp'=> "Never give out your credit card info to an untrusted source. Scammers could user your Facebook likes to trick you into parting with your money.",
		'vars'=> array(
			"fb.likes.data:random.name"
		),
		'layout'=> "scenario",
		'type'=> "text",
		'social'=>'TMI showed me how a crook could "phish" for my credit card info by posing as one of the brands I follow on social. Play TMI to find out if you can be scammed.'
	),

	//14
	array(
		'q'=> "Did you know you have a Facebook ID? Yours is %s.",
		'exp'=> "You can't change or eliminate this ID. It's just one more piece of Internet data that might help a crook access your personal information.",
		'vars'=> array(
			'fb.id'
		),
		'layout'=> "scenario",
		'type'=> "text",
		'social'=>"I have a Facebook ID number? I had no idea. Could a thief use this info to target you? Play TMI to find out."
	),

	//15
	array(
		'q'=> "I see you're going to %s. Does that mean no one will be home at your house?",
		'exp'=> "Addresses are available from some sources on the internet, and a determined thief could figure out yours. Public RSVPs let the world know where you'll be, as well as when you won't be at home.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "event",
		'in_past'=>false,
		'social'=>"Yikes! If TMI knows when I'll be away from home thanks to my Facebook RSVPs, so does a criminal. Are you RSVPing for trouble? Play TMI and find out."
	),

	//16
	array(
		'q'=> "Which one of your %s friends should I pretend to be in order get information about you?",
		'exp'=> "Online thieves might see your friend list as an important resource. The list can tell them the answer to a security question, or help them find other potential victims.",
		'vars'=> array(
			'fb.friends.data'
		),
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "count",
		'social'=>"I didn't realize online thieves might use my friend list to find new targets. Are your friends safe? Play TMI to find out."
	),

	//17
	array(
		'q'=> "You came into this world on %s. You know what they say about %s!",
		'exp'=> "Your birthday is a crucial piece of information that can help thieves access an array of information about you, including your finances.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "birthday",
		'signs'=>array(
			'321_420'=>array( 'name'=> 'Aries', 'quality'=> 'adventurous' ),
			'421_521'=>array( 'name'=> 'Taurus', 'quality'=> 'Patient' ),
			'522_621'=>array( 'name'=> 'Gemini', 'quality'=> 'Adaptable' ),
			'622_722'=>array( 'name'=> 'Cancer', 'quality'=> 'Emotional' ),
			'723_821'=>array( 'name'=> 'Leo', 'quality'=> 'Generous' ),
			'822_923'=>array( 'name'=> 'Virgo', 'quality'=> 'Modest' ),
			'924_1023'=>array( 'name'=> 'Libra', 'quality'=> 'Deplomatic' ),
			'1024_1122'=>array( 'name'=> 'Scorpio', 'quality'=> 'Determined' ),
			'1123_1222'=>array( 'name'=> 'Sagittarius', 'quality'=> 'Optimistic' ),
			'-8_120'=>array( 'name'=> 'Capricorn', 'quality'=> 'Practical' ),
			'121_219'=>array( 'name'=> 'Aquarius', 'quality'=> 'Friendly' ),
			'220_320'=>array( 'name'=> 'Pisces', 'quality'=> 'Imaginative' ),
		),
		'social'=>"Wow! It was so easy for TMI to discover my birthdate. Is yours safe from online thieves? Play TMI to find out."
	),

	//18
	// Impossible question about friends from same high school

	//19
	array(
		'q'=> "Well, hello, Mr./Mrs. %s.",
		'exp'=> "Once an online criminal knows where you work, they could guess how much you make and try to go straight for the jugular (a.k.a., your bank account).",
		'layout'=> "scenario",
		'type'=> "text",
		'vars'=> array(
			'fb.work.employer.name'
		),
		'social'=>"Yikes! I don't want strangers to know where I work, but TMI figured it out right away. Is your personal info secure? Play TMI to find out."
	),

	//20
	array(
		'q'=> "You and %s sure look happy. Maybe I'll pretend to be %s next.",
		'exp'=> "The people you love can give a potential thief a range of unintended gifts, from the answer to a security question to the identity of their next victim.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> 'significant_other',
		'social'=>"Hey friends, TMI knows who you are! I didn't realize online thieves might use my friend list to find new targets. Play TMI to find out how."
	),

	//21
	array(
		'q'=> "Something tells me you might bank at %s. Guess I know where I'll go to make my next withdrawal.",
		'exp'=> "Telling a crook where you bank is like telling them where your spare house key is located. Do anything you can to keep this info private.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> 'bank_search',
		'social'=>'When I "liked" a financial institution, I didn\'t realize I might be telling a thief where to find my money. Play TMI to find out if your money is safe.'
	),

	//22
	array(
		'q'=> "I see you manage %s. I wonder how hard it would be to hack in there and ask everyone to send me $20?",
		'exp'=> "A thief will use whatever is at his or her disposal to get access to personal information or funds.",
		'layout'=> "scenario",
		'type'=> "text",
		'vars'=> array(
			'fb.accounts.data.name'
		),
		'social'=>"If you manage more than one Facebook page, that information could help a thief connect important dots. Play TMI to find out what a crook can discover about you."
	),

	//23
	array(
		'q'=> "If I can find out that you were in %s on %s, imagine who else can.",
		'exp'=> "Do you really want a potential crook to know where you've been and when you were there?",
		'layout'=> "scenario",
		'type'=> "text",
		'vars'=> array(
			'fb.tagged_places.data.name',
			'fb.tagged_places.data.created_time'
		),
		'social'=>"I wanted to share my check-in with YOU, not a potential criminal. Play TMI and find out what geolocation services are revealing about you."
	),

	//24
	array(
		'q'=> "You're a fan of %s? You obviously have fantastic taste in TV.",
		'exp'=> "No piece of info is too small to be useful to an online thief. Everything you \"like\" helps them size you up.",
		'layout'=> "scenario",
		'type'=> "text",
		'vars'=> array(
			'fb.television.data.name'
		),
		'social'=>"Until I played TMI, I didn't realize how much what I follow online gives a thief a picture of who I am. Play TMI to find out how a thief could impersonate you."
	),

	//25
	//This is dis-allowed for apps that aren't real FB clients
	array(
		'q'=> "You have a message waiting from %s. I'm happy to answer it for you if you're too busy.",
		'exp'=> "Think what you do online is private? Think again. Even seemingly innocuous actions could be used by online thieves. ",
		'layout'=> "scenario",
		'type'=> "text",
		'vars'=> array(
			'fb.inbox.data.comments.data.from.name'
		),
		'social'=>"Messages are supposed to be private, but TMI sees who is messaging me. Play TMI to find out how much a potential thief can easily discover about you."
	),

	//26
	// Impossible question using Pinterest

	//27
	array(
		'q'=> "We found you on %s with the username %s.",
		'exp'=> "When it comes to online scammers, it's possible that everything you share might be used against you. So be careful what you share.",
		'vars'=> array(
			"fc.socialProfiles.type",
			"fc.socialProfiles.username"
		),
		'layout'=> "scenario",
		'type'=> "text",
		'social'=>"The TMI Game knows all my social media profiles. That could help a thief steal my identity. Play TMI to find out how to play it safe online."
	),

	//28
	array(
		'q'=> "Are you related to %s?",
		'exp'=> "Finding someone's family members online is a piece of cake. This information can help a thief answer a security question or pose as a relative who is stuck in Spain and needs to \"borrow\" money.",
		'layout'=> "qa",
		'type'=> "custom",
		'fnc'=>"matching_name",
		'social'=>"TMI discovered the names of my relatives. Have you ever used a relative's name in a password? Play TMI to find out how easy it is for a crook to find your relatives."
	),

	//29
	array(
		'q'=> "You've got a way with 140 characters: %s",
		'exp'=> "Your social profiles tell the story of your life--and that story may give a potential crook the info they need to break into an account or impersonate you online.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "twitter",
		'social'=>"The TMI Game knows all my social media profiles. That could help a thief steal my identity. Play TMI to find out how to play it safe online."
	),

	//30
	// TODO: Generation question

	//31
	array(
		'q'=> "Looks like you checked in at %s",
		'exp'=> "Public check-ins let people know where you bank, shop, and work. A thief may even be able to draw a pattern that spells out when you aren't home.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> 'places',
		'social'=> "I wanted to share my check-in with YOU, not a potential criminal. Play TMI and find out what geolocation services are revealing about you."
	),

	//32
	array(
		'q'=> "%s look like a lot of fun.",
		'exp'=> "You may think your friend list is nothing to keep private, but it can be a perfect pond for thieves looking for somewhere to go \"phishing.\"",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "photos_of_friends",
		'social'=>"Hey friends, TMI knows who you are! I didn't realize online thieves might use my friend list to find new targets. Play TMI to find out."
	),

	//33
	array(
		'q'=> "Is %s your brother? Have you ever used their name or birthdate in password?",
		'exp'=> "The names of relatives can help online thieves hack into your account. Still want to list all your relatives online?",
		'layout'=> "qa",
		'type'=> "custom",
		'search'=> 'brother',
		'fnc'=> "family_member",
		'social'=>"TMI discovered the names of my relatives. Have you ever used a relative's name in a password? Play TMI to find out how easy it is for a crook to find your relatives."
	),

	//9-b
	array(
		'q'=> "I see you have a child named %s. Is it true that a parent's greatest weakness is their child?",
		'exp'=> 'A potential crook can use your child\'s name to answer a security question or pose as them for a "phishing" scam.',
		'layout'=> "scenario",
		'type'=> "custom",
		'search'=> 'daughter',
		'fnc'=> "family_member",
		'social'=>"TMI discovered the names of my relatives. Have you ever used a relative's name in a password? Play TMI to find out how easy it is for a crook to find your relatives."
	),

	//34
	array(
		'q'=> "\"Dear %s, I'm stuck in Asia without a passport. I need $5,000 to get home. Can you please wire me some money? Thanks, %s.\"",
		'exp'=> "\"Phishing\" is when a criminal pretends to be a trustworthy source in order to gain access to your sensitive info. Your Facebook friend list is a big help.",
		'layout'=> "scenario",
		'type'=> "text",
		'vars'=> array(
			'fb.first_name',
			'fb.friends.data.name'
		),
		'social'=>"I got \"phished\" by TMI! Play TMI to find out how crooks could use your social profiles to pull off a common scam."
	),

	//35
	array(
		'q'=> "I see %s is all over your Facebook page. Is he or she your child? Best friend? The answer to your banking security question?",
		'exp'=> "Some people use the names of friends and family members as the answers to online security questions. By broadcasting this information, you're helping them figure out what's important to you.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "most_frequent",
		'social'=>"TMI discovered the names of my relatives. Have you ever used a relative's name in a password? Play TMI to find out how easy it is for a crook to find your relatives."
	),

	//36
	array(
		'q'=> "It doesn't take a genius to see you're using a %s.",
		'exp'=> "Once a thief knows what kind of device you use, they can figure out the best way to break in and steal your info.",
		'vars'=> array(
			"fb.devices.data.hardware"
		),
		'layout'=> "scenario",
		'type'=> "text",
		'social'=>"The TMI Game knows what device I'm playing on. A thief could use that info to steal my identity. Play the TMI Game and see if it can guess what device you're on."
	),

	//37
	array(
		'language'=> array(
			'spanish'=> "¿Eres fluidez en la privacidad en línea?",
			'arabic'=> "أنت يجيد الخصوصية على شبكة الإنترنت؟", //rtl
			'armenian'=> "Դուք Տիրապետում է առցանց գաղտնիության.",
			'chinese'=> "你能说流利的在线隐私？",
			'czech'=> "Jste plynně v online soukromí?",
			'danish'=> "Er du flydende i online-privatliv?",
			'dutch'=> "Ben je vlot in online privacy?",
			'english'=> "Are you fluent in online privacy?",
			'filipino'=> "Sigurado ka matatas sa online privacy?",
			'finnish'=> "Oletko sujuvasti verkossa yksityisyyttä?",
			'french'=> "Parlez-vous couramment dans la vie privée en ligne?",
			'galician'=> "Vostede fluente en privacidade en liña?",
			'georgian'=> "ხართ თუ არა ფლობს ონლაინ კონფიდენციალურობის?",
			'german'=> "Sind Sie fließend in Online-Privatsphäre?",
			'greek'=> "Είστε άπταιστα σε απευθείας σύνδεση προστασία της ιδιωτικής ζωής;",
			'hebrew'=> "האם אתה שולט בפרטיות באינטרנט?", //rtl
			'hindi'=> "आप ऑनलाइन गोपनीयता में धाराप्रवाह हैं?",
			'hungarian'=> "Ön beszél online privacy?",
			'icelandic'=> "Ertu altalandi á netinu næði?",
			'indonesian'=> "Apakah Anda fasih dalam privasi online?",
			'irish'=> "An bhfuil tú líofa i príobháideachta ar líne?",
			'italian'=> "Sei fluente in privacy online?",
			'japanese'=> "あなたがオンラインプライバシーに堪能ですか？",
			'korean'=> "당신은 온라인 개인 정보 보호를 유창하게 구사입니까?",
			'latvian'=> "Vai esat brīvi tiešsaistes privātumu?",
			'norwegian'=> "Er du flytende i personvern på nettet?",
			'polish'=> "Czy jesteś biegły w prywatności w Internecie?",
			'portuguese'=> "Você fluente em privacidade online?",
			'russian'=> "Вы свободно говорит на конфиденциальности в Интернете?",
			'serbian'=> "Да ли сте течно у онлине приватности?",
			'slovak'=> "Ste plynne v online súkromie?",
			'slovenian'=> "Ali ste tekoč v zasebnosti na spletu?",
			'swedish'=> "Är du flytande i integritet online?",
			'thai'=> "คุณมีความชำนาญในความเป็นส่วนตัวออนไลน์ได้หรือไม่",
			'turkish'=> "Online gizlilik akıcı?",
			'ukrainian'=> "Ви вільно говорить на конфіденційності в Інтернеті?",
			'vietnamese'=> "Bạn có thông thạo riêng tư trực tuyến?",
			'welsh'=> "A ydych yn rhugl mewn preifatrwydd ar-lein?",
			'yiddish'=> "זענען איר גלאַט אין אָנליין פּריוואַטקייט?",//rtl
		),
		'rtl'=> array('arabic','hebrew','yiddish'),
		'q'=> "That's great that you speak %s! Are you fluent in online privacy?",
		'exp'=> "Everything you share online is a possible entry point for thieves. For instance, your second language might inadvertently tell them your favorite vacation spot, which is a common security question.",
		'type'=> "custom",
		'fnc'=> "language",
		'social'=>"I thought I was just sharing, but I learned that my second language can help thieves plan the perfect crime. Play TMI to find out if you're oversharing."
	),

	//38
	// Delete : RSVP attended

	//39
	// Impossible: Instagram integration

	//33-b
	array(
		'q'=> "Is %s your sister? Have you ever used their name or birthdate in password?",
		'exp'=> "The names of relatives can help online thieves hack into your account. Still want to list all your relatives online?",
		'layout'=> "qa",
		'type'=> "custom",
		'search'=> 'sister',
		'fnc'=> "family_member",
		'social'=>"TMI discovered the names of my relatives. Have you ever used a relative's name in a password? Play TMI to find out how easy it is for a crook to find your relatives."
	),

	//40
	array(
		'q'=> "I haven't had a fake ID this convincing since the summer before senior year.",
		'exp'=> "All it may take is a profile photo, current address, and a decent printer for a criminal to create a fake ID that could potentially allow them to open a credit card or other account in your name.",
		'layout'=> "scenario",
		'type'=>'custom',
		'fnc'=> "image",
		'vars'=> array(
			'fb.picture.data.url'
		),
		'social'=>"My profile is private, but TMI knows what languages I speak. Online thieves could use that as ammo. Play TMI to find out if you're oversharing."
	),

	/*
	//Removed Questions
	array(
		'q'=> "Love the recent pictures. Every picture really does tell a story.",
		'exp'=> "Your Facebook likes help online thieves paint a picture of who you are. No piece of info is too small to be useful.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "image",
		'vars'=> array(
			'fb.photos.data.source'
		),
		'social'=>""
	),

	array(
		'q'=> "\"%s\" Rule #1 about the Internet: it never forgets a thing.",
		'exp'=> "If you've said anything in the media, chances are it's available online. Don't let your past words or actions come back to haunt you--or give criminals info they can use to impersonate you.",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "status"
	),

	array(
		'q'=> "I feel like I know you from somewhere? ",
		'exp'=> "Every picture tells a story… and once a thief knows your story, he or she can write a not-so-happy ending. ",
		'layout'=> "scenario",
		'type'=> "custom",
		'fnc'=> "photoset"
	),
	//Removed Questions
	*/

	)//endlist
);