module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    assets: "public/assets/",
    fuel: "fuel/app/",
    bower: "bower_components/",

    sass: {
      options: {
        includePaths: [
          'scss/',
          '<%=bower%>foundation/scss',
          require('node-bourbon').includePaths
        ],
        imagePath: "./"
      },
      dist: {
        options: {
          outputStyle: 'compressed'
        },
        files: {
          '<%= assets %>css/app.css': 'scss/app.scss'
        }
      }
    },

    uglify: {

      vendor: {
        options: {
          sourceMap: true,
        },
        files: [{
          '<%=assets%>js/vendor.js': [
            '<%=bower%>jquery/dist/jquery.min.js',
            '<%=bower%>jquery.cookie/jquery.cookie.js',
            '<%=bower%>jquery-placeholder/jquery.placeholder.min.js',
            '<%=bower%>jquery.transit/jquery.transit.js',

            //'<%=bower%>modernizr/modernizr.js',
            //'<%=bower%>fastclick/lib/fastclick.js',

            '<%=bower%>foundation/js/foundation/foundation.js',
            '<%=bower%>foundation/js/foundation/foundation.tooltip.js',
          ]
        }]
      },

      frontend: {
        options: {
          sourceMap: true,
        },
        files: [{
          dest: '<%= assets %>js/frontend.js',
          src: ['js/*.js', 'js/**/*.js']
        }]
      }
    },

    imagemin: {
      dist: {
        options: {
          optimizationLevel: 3
        },
        files: [{
          expand: true,
          dest: "<%=assets%>img/",
          cwd: 'images',
          src: ["**/*.{png,jpg,gif}", "!**icons/**", ]
        }]
      }
    },

    webfont: {
      icons: {
        src: 'images/icons/*.svg',
        dest: "<%=assets%>/font/",
        destCss: "scss",
        options: {
          engine: "node",
          stylesheet: "scss",
          relativeFontPath: '../font/',
          htmlDemo: false
        }
      }
    },

    watch: {
      grunt: {
        files: ['Gruntfile.js'],
        tasks: ['all']
      },

      sass: {
        files: ['scss/**/*.scss', 'scss/*.scss'],
        tasks: ['sass']
      },

      js: {
        files: 'js/**/*.js',
        tasks: ['newer:uglify']
      },

      images: {
        files: ['images/**/*.{png,jpg,gif}', 'images/*.{png,jpg,gif}'],
        tasks: ['newer:imagemin']
      },

      icons: {
        files: ['images/icons/**/*.svg'],
        tasks: ['webfont']
      }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-webfont');
  grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('all', ['imagemin', 'webfont', 'sass', 'uglify']);
  grunt.registerTask('default', ['all', 'watch']);
};